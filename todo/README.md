---
Title: TODO List
Subtitle: RWX Stuff That Needs Work
---

Here are the upcoming planned changes and new content to RWX.GG. This TODO list is maintained instead of an issues system on GitLab only because it is far easier to maintain and regularly access. As priority of TODO items changes they are elevated to the top. Past items are removed entirely or transferred to the [Changes](/changes/) overview.

1. Change default CSS to be dark mode.

1. Test on Android device.

1. Review all for consistent use of FAQ styling

1. Add CSS styles for `.see` so that bullets can be used for Lynx, etc.

1. Update [main.js](/assets/main.js) to progressively enhance any URLs pointing to duckduckgo lite version such that they are change do remote `lite` and point to full version

1. Write an `audit` script that combines [check](/check) with validation of consistent use of [callouts](/styleguide/) and any other style guide violations that might creep in. Have check only run on files that have changed since the last run of `check`.

1. Add *spoiler* mode that will automatically hide all the code rather than give away the commands so that it can be used to review as you go.

1. Check all `sh` code blocks to ensure they shouldn't be `bash` instead. Check `pandoc --list-highlight-languages` to be sure of others.

## Long Term

1. Replace [TLCL](/books/tlcl/) with *Linux Terminal Mastery*, a knowledge app that can double as a book, PDF, or whatever. The world needs a *true* terminal mastery book that weaves together deep knowledge and skill in using the fastest human-computer interface physically possible. The rather shallow and inexperienced "Keyboard Tricks" chapter in TLCL was really the trigger. It is disastrously bad and uninformed.
