---
Title: Computer Languages
Subtitle: Including the Binary Language of Moisture Evaporators
tpl-hdsearch: true
---

A *computer language* usually refers to a programming or markup language. These languages have grammar including [syntax](/syntax/) just like other natural human languages. In fact, those who study foreign language tend to pick up new programming languages easier as well, and vice versa.

## Major Categories

Computer languages have two major categories:

--------------  -------------------------------------- 
 Category       Description
--------------  -------------------------------------- 
 Imperative     Contain instructions or
                procedures telling the computer what
                to do and *how* to do it requiring
                the computer to remember what it is
                doing as it does it called *state*.

 Declarative    Contain descriptions of *what* is
                wanted from the computer by describing
                it rather then telling it what to do.

 Data           Data languages are not programming at
                all but critically important since
                they contain structured data used
                by other languages. There is often
                a fine line between declarative and
                data languages.
--------------  -------------------------------------- 

Table: Major Computer Language Categories

One of the biggest mistakes in the minds of computer programmers is assuming that *only* imperative programming *is* programming. You see mistakes even in curriculums like Harvard that state emphatically that HTML is not a *programming* language, which is categorically wrong, it is, it's a *declarative* programming language.

:::co-fyi
There are dozens of other [computer language  paradigms](https://duck.com/lite?q=computer language  paradigms). These are just the two main categories. :::

## Recommended Languages to Learn{#recommended}

The following nine languages provide the most personal empowerment and overall knowledge of how languages work so that you can pick up *any* language later more easily.

------------- --------------- ---------------------------------------------------
 Language      Category       Description
------------- --------------- ---------------------------------------------------
 JSON          Data           Human-*readable* structured data language used for 
                              inter-applications communication.

 YAML          Data           Human-*writable* JSON-compatible structured data
                              language for configuration and database-free
                              information storage. Used for
                              [Markdown document front-matter](https://duck.com/lite?q=Markdown document front-matter).

 Markdown      Declarative    Primary knowledge source language developed
                              by writers, for writers which can be easily 
                              written with nothing but the simplest text editor
                              and rendered in any other format with tools like
                              [Pandoc](/pandoc/).

 HTML          Declarative    Web *content* language invented by physicists
                              requiring coding skills to write.

 CSS           Declarative    Web *style* language added when HTML started
                              adding appearance elements instead of content
                              and structure.

 JavaScript    Imperative     Web *logical* language that allows imperative
                              logic (if-then) and event (when-then) handling. 
                              
 Bash          Imperative     Default shell language for Linux. Use
                              from the [command line](/cli/) or as a
                              [script](https://duck.com/lite?q=script).

 C             Imperative     Mother of all languages, literally. Originally 
                              created to rewrite the first [Unix](/unix/)
                              operating system. Good to understand all languages.

 Go            Imperative     Modern C replacement for most work created at
                              Google by "Bell Labs refugees" including Ken
                              Thompson (the creator of Unix) and Rob Pike (the
                              co-creator of modern Unicode standard).
------------- --------------- ---------------------------------------------------

:::co-mad
You will find that this selection of languages differs significantly from what most traditional educational organizations --- and even modern bootcamps --- would have you learn. This is by design. Many of the languages taught to beginners are ancient and focus on things like single object inheritance that would get you fired from most jobs as a software developer today. It is no secret that these organizations and bootcamps are *not* keeping up. They are teaching ancient languages that few in the industry today would pick for [greenfield projects](https://duck.com/lite?q=greenfield projects). Sure these [legacy languages](https://duck.com/lite?q=legacy languages) will be around for a very long time. That doesn't mean you should learn them as your first languages, at all. 
:::

## Resources

[HTML *IS* a Programming Language](https://youtu.be/4A2mWqLUpzw){.see}
