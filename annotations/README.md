---
Title: Annotations
Subtitle: Building on Books
tpl-hdsearch: true
---

*Annotations* are the most sustainable way to supplement and support [book](/books/) learning. Books are comprehensive, but very often incomplete, outdated, or just plain wrong. Rather than throw out all the great information in the best books, annotations provide an opportunity for a group or individual to leverage the format and structure of the book to add critical explanations and additions to avoid any gaps in learning that would result from learning from the book alone.

:::co-example
The [Head First Go](/books/hfgo/) book completely omits anything about `go mod init`, the *standard* way to begin any Go development these days. If you just learned Go from this book you would have critical gap in your learning.
:::
