---
Title: Hyper Text Markup Language / HTML
Subtitle: The *Content* of the Web
tpl-hdsearch: true
---

*HTML* is the main [language](/languages/) for the [World Wide Web](https://duck.com/lite?q=World Wide Web). Every [web site](https://duck.com/lite?q=web site) has some HTML in it. HTML is one of the most important coding language anyone will learn because it is so [ubiquitous](/ubiquitous/). Every single technical occupation requires some knowledge of HTML.
