---
Title: Visual Studio Code / VSCode
tpl-hdsearch: true
---

[*Visual Studio Code*](https://code.visualstudio.com), as Microsoft calls it, or *VSCode* to the rest of us, is currently the leading [GUI](/gui/) [editor](/editor/). VSCode runs on top of the [Electron](/electron/) platform created by [GitHub](/github/) to power their [Atom](/atom/) editor. Like the rest of the minimal GUI editors to appear VSCode was heavily inspired from Atom and originally [Sublime](/sublime/).

:::co-warning
Do not confuse *Visual Studio Code* with *Visual Studio*.
:::
