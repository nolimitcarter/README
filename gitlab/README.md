---
Title: GitLab
Subtitle: World's *Best* Git Hosting Service
tpl-hdsearch: true
---

GitLab is a [far superior](isbest/) alternative to [GitHub](/github/) mostly because of its independent and [open source](/opensource/) nature and [DevOps](/devops/) built in integration, but also because of its pure and elegant [UX](/ux/).

