---
Title: Choose GitLab Over GitHub
Subtitle: Why GitLab Objectively Beats GitHub at Every Level
tpl-hdsearch: true
---

GitLab is the recommended Git hosting provider for this course. GitLab provides an objectively better service and gives back far more to the community than GitHub. All of us are all still required to have [GitHub](/github/) accounts to stay connected to the 10 million legacy users stuck on it but use GitLab to host your repos, public and private.

------------------------------------------------------------------------------------------------
GitLab                                          GitHub
----------------------------------------------  ------------------------------------------------
MIT open source license.                        Proprietary closed source.

Accepts community contributions.                No public contributions.

Full CI/CD DevOps from beginning.               Depends on third-parties.

Everything is free.                             Organizations and more not free.

Can be installed at home or on site.            Cannot be installed.

More likely to use on the job.                  More likely to use for hobbies.

Multiple groups and subgroups.                  No sub-organizations.

Containerized project repos.                    Less secure repos share resources.

Agile progressive architecture.                 Massive technical debt.

New repos pushed with git push.                 Repos created using web or API.

Powerful import, export, mirroring.             No mirroring whatsoever.

Cleaner Vue-based user interface.               Ancient bootstrap-based design.

Groups and projects get icons.                  No icons except for users.

Powerful WebIDE.                                Broken web editor.

Private company.                                Owned by Microsoft.

Open core business model.                       Traditional closed business model.

100% corporate transparency.                    Zero corporate transparency.

Promotes remote work-from-home.                 Requires most on-site.

Clean business history.                         Multiple harrassment scandals.

800,000 better users (2019).                    10 million legacy users (2019).

Nice animated fox logo.                         Lame Octocat logo.
------------------------------------------------------------------------------------------------

Table: GitLab's Objective Superiority to GitHub

## Resources

[Choose Gitlab](https://youtu.be/7CZSv3t3ORA){.see}

