---
Title: Learning to Work and Learn
tpl-hdsearch: true
---

> "Live in the question. Don't be satisfied with quick answers. Questions lead us on a quest. Seek your own answers and not just what other people tell you. Become the authority of your own life [and learning]." (Larry Schultz)

Learning is fundamental. Whatever your views are on life and its purpose, it's likely that learning --- the process of gaining skills, knowledge, and experience --- is a fundamental part of it. Perhaps you believe learning is the main goal of life. Maybe you simply seek enjoyment and simply enjoy learning. Maybe you even seek to inflict pain on others and *still* depend on learning how to do it. Learning is fundamental, but it is also a skill that itself must be learned and mastered. These days learning requires [reading, writing, and executing (or experiencing)](/rwx/) (RWX), which are the primary attributes of a true [autodidact](/autodidact/).

## Resources

[Learning How to Learn](https://www.coursera.org/learn/learning-how-to-learn){.see}
[Mindshift: Break Through Obstacles to Learning and Discover Your Hidden Potential](https://www.coursera.org/learn/mindshift){.see}

