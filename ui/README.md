---
Title: User Interface
tpl-hdsearch: true
---

You are a *user* when you use a computer and you connect yourself to the computer through an *interface*. Hence the term *user interface* or *UI*. The two most common references to user interfaces are as follows:

* [Graphic User Interfaces](/gui/)
* [Terminal User Interfaces](https://duck.com/lite?q=Terminal User Interfaces)
* [Command Line User Interfaces](/cli/)
* [Conversational User Interfaces](https://duck.com/lite?q=Conversational User Interfaces)

The field of [HCI](/hci/) studies different ways humans can interact with devices.
