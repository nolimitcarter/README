---
Title: Computer Process
tpl-hdsearch: true
---

A *process* is simply a running program started either directly by a [user](/users/) or indirectly by the system itself.

:::co-tip
Use the `top` command to see the running processes in real time.
:::
