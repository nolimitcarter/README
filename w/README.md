---
Title: W is for Writing
---

Give your brain space.

Brain is not for storage.

### Discussion Questions

* What are some reasons we should write what we learn?
* How does writing "give your brain space"?
* What do you think of the statement "your brain is not for storage"?
* What methods of writing are the best for learning?
* Does note-taking writing?
* How do you write? 
