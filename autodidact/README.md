---
Title: Becoming an Autodidact
Subtitle: More Than Just Life-Long Learning
tpl-hdsearch: true
---

An *autodidact* is one who takes learning into their own hands either because they prefer it or are forced to for lack of other resources. De Vinci, Galileo, Ada Lovelace, Tesla, Wozniak were all autodidacts. All have followed a [simple formula](/rwx/). An autodidact reads, writes meticulous notes, and executes tests to gain new knowledge and skills. Making mistakes is an integral part of the learning process. Autodidacts don't fear mistakes or feel shame for them. Instead they seek them, write about them, share them, and [fail as fast as possible](/failfaster/) *never giving up* until they eventually succeed.

Becoming an autodidact is the most essential skill anyone will ever master. Without it you simply cannot progress on your own. So many enter the workplace without learning this important skill. Perhaps you have encountered them. They tend to come over and ask a lot of questions and usually come off as needy and unprofessional. Don't be that person. Learn to do your own research, answer your own questions, and really *look* at things.

:::co-rage
Ironically few traditional schools spend any time at all helping you become an autodidact. It is assumed that you naturally will learn it as you are thrown into the traditional educational environment where you learn it or die. It is almost as if such organizations were motivated to make you dependent on *them* instead of yourself.
:::

:::co-fun
Those who study humanity say that the collective ability to pass on knowledge to the next generation even after we have died is the single greatest reason humans have rocketed past all other species in growth and progress. Whether you believe this is because of natural science or some greater metaphysical force it is a fact we can observe.
:::

## Resources

[Deep Work](https://www.amazon.com/Deep-Work-Focused-Success-Distracted/dp/1455586692){.see}
