---
Title: Database as a Service
Subtitle: DaaS
tpl-hdsearch: true
---

*Database as a service* allows queries that would normally require a database installation (and a host server to run it on) to be done securely over the Internet. [FaunaDB](https://fauna.com) is one example of DaaS.

[Cloud Services](/cloud/){.see}
