---
Title: JAMstack
Subtitle: The Static Web Returns, But Better
tpl-mdhead: true
---

> "[JAMstack is] a modern web development architecture based on client-side JavaScript, reusable APIs, and prebuilt Markup" — Mathias Biilmann (CEO & Co-founder of Netlify).

JAMstack is what you get when you live in a world of vast content delivery networks that put your content on servers as close as possible to the people using it combined with  [web services](/services/) for everything you could imagine ever needing some code on a backend server to run. In other words, there's no need to own and maintain your own [server](/serverless/) when you can use a [service that provides them for you](/paas/).

## Resources

<https://jamstack.org>{.see}
<https://jamstack.wtf>{.see}

