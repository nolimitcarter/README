---
Title: IT Cloud Services
Subtitle: Pay Someone Else to Take Care of It
tpl-hdsearch: true
---

Services are provided over the Web and allow you to do things that once were only possible from rather large IT shops. There are *lots* of things *as-a-service* offerings today. Collectively these are commonly referred to as "cloud" services.

[Software as a Service](/saas/){.see}
[Platform as a Service](/paas/){.see} 
[Infrastructure as a Service](/iaas/){.see} 
[Function as a Service](/faas/){.see}
[Database as a Service](/daas/){.see}
