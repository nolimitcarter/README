---
Title: Function as a Service
Subtitle: FaaS
tpl-hdsearch: true
---

*Function as a service* is when you just want to input some arguments, perform some action, and get a resulting value but you don't want to do all the work to host the [infrastructure](/iaas/), [platform](/paas/) to do so and you don't want to just use someone else's [software](/saas/) --- just like when creating [functions when programming](https://duck.com/lite?q=functions when programming) except you do it over the Internet on a service that hosts your functions and runs them when you call them on demand. Amazon calls this their Lambda service. Others call this "actions."

[Cloud Services](/cloud/){.see}
