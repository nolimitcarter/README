---
Title: Human Computer Interaction / HCI
Subtitle: How Humans Connect to Their Devices
tpl-hdsearch: true
---

The scientific field of *human computer interaction* or *HCI* studies how humans connect and interact with computer devices through any of a number of current [user interfaces](/ui/) as well a potential future interfaces.

:::co-fyi
Elon Musk famously called all present user interfaces, "ridiculously slow" and has put forth theories about how a "neural mesh" might be used to help humans to keep up with the pace of progress machines --- and specifically artificial intelligence --- is making so that such intelligences don't one day view us "in the most benign scenario" as "pets".
:::


