---
Title: Bash Shell
Subtitle: The Standard Command Line of Linux
tpl-hdsearch: true
---

*Bourne Again Shell* is default interactive [user](/users/) [shell](/shell/) of the Linux operating system. It is also a powerful [scripting language](/languages/) known for creating useful scripts complex single-line commands quickly. As such it has a rather unusual [syntax](/syntax/) when compared to other languages. Bash usage and programming are a [primary focus](/books/tlcl/) of the [Linux Beginner Boost](/schedule/).
