---
Title: Markdown
Subtitle: Invented by Writers, Not Scientists
tpl-hdsearch: true
---

Markdown is a simple [markup](/markup/) language invented by writers who didn't want to write [HTML](/html/). Markdown has become the standard for most technical writing and is supported on Reddit, StackExchange, Discord, GitLab, GitHub, and pretty much everywhere else.

-------------------------------------- ----------------------------------------------
 [Basic Markdown](/basicmark/)          Emphasis on simplicity and compatibility
 [CommonMark](/commonmark/)             General industry standard (but no tables)
 [Pandoc Markdown](/pandocmark/)        Maximum power and flexibility, no HTML deps
-------------------------------------- ----------------------------------------------

:::co-fyi
Modern companies such as GitLab depend *heavily* on Markdown for everything they do. Watch any of their release conference videos to see examples of all the documentation and presentations that make extensive use of Markdown (specifically [CommonMark](/commonmark/) since GitLab as an organization does not have a need for the Math and other advantages of Pandoc Markdown.)
:::

## Types of Markdown

Eventually you'll want to use [Pandoc Markdown](/pandocmark/) for everything. (In fact, [this document](https://gitlab.com/rwx.gg/README/-/tree/master/md) is written in it.) Pandoc Markdown is far and away the most powerful, sustainable, and supported format for capturing knowledge source, which is why the R language project adopted it as their language documentation format.

However, you can learn [basic Pandoc Markdown](/basicmark/) in less than 30 minutes and it will work *everywhere*.

Why the differences?

The original Markdown was never standardized and has evolved into more than a dozen different flavors many of which are incompatible with one another. Thankfully there are only three versions that really matter:

## Standard Knowledge Source Language

Markdown has become the closest thing to a universal standard for writing all documentation today. In addition, authors write entire textbooks and novels in Markdown these days. When combined with a tool like [Pandoc](/pandoc/) your Markdown can be converted to *every other format on the planet*. Markdown is the *only* markup writing language that can claim this. Therefore, *everyone* should learn it from those needing a solid, sustainable way to take notes, to those creating entire [knowledge bases](/kbase/). Ironically Markdown is practically not taught at all today in any educational setting.

[What about wikis?](/wikis/){.see}

