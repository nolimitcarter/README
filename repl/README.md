---
Title: Read Evaluate Print Loop / REPL
tpl-hdsearch: true
---

A *REPL* is simply a command line. We tend to think of Bash (or another interactive user shell) as the *only* command line but there are many. Python, Ruby, and Node all have their own REPL command lines. In fact, they are so common that this acronym was chosen as the name of the <https://REPL.it> coding sandbox site where you can try out over sixty different languages most of which are from the command line.
