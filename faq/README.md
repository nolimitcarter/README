---
Title: Frequently Asked Questions (FAQ)
---

Here are the most common questions people ask about this course and [app](/pka/). There is another list of [stupid bad advice](/stupid/) people will tell you.

* [What are the prerequisites?](/prereqs/)
* [How many hours per week do I need to set aside?]
* [Why *boost*?]
* [Will this course get me job?]
* [Will I learn data structures and algorithms?]
* [What is an *open course*?](/opencourse/)
* [What about the code? Can I use it in my own projects?]
* [Can I run my own beginner boost?]
* [Does this have anything to do with Boostnote?]
* [Where can I get the source of this?]
* [Which tools, apps, and services should I use?](/essentials/)

## How many hours per week do I need to set aside? 

About 40 hours a week, not unlike a traditional coding bootcamp. However, that depends on you. Some weeks will have a higher reading requirement that others. Some weeks will require more independent coding and research. You might even do all the reading and activities *well* in advance.

By the way, if you can't maintain that pace remember that the course is rotated and restarts about every 16 weeks. You can always "drop out" and prepare for the next one. There is *never* any reason to beat yourself up about your own person pace. Remember *you* are in charge of your learning.

## Why *boost*?

Because you are getting a *boost* (not the boot).

The word *bootcamp* invokes all kinds of emotion and opinion from different people. The word has come to refer to an intensive learning experience over what is traditionally 12-week period. Bootcamps are currently looked down upon by most in the tech industry because a bootcamp along is *rarely* enough to good enough for *any* full-time occupation, but bootcamps are an amazing start because of the intensity, focus, energy, and community involved. 

In short, there shouldn't be *any* expectation in anyone's mind that what people get from this course will have anything to do with whatever they think a "bootcamp" is. It's not. This course is meant --- above all --- to boost beginners on their journey toward other tech learning and occupations. It may be intensive and cover required essential skills, but it's not enough. Anyone from the industry would agree there is a *lot* more to learn before fulfilling the requirements of any of the [target occupations](/occupations/), which all take another 2000 hours or so of learning to master even at a beginner level. Don't let anyone tell you otherwise. There are no short-cuts.

## Will this course get me job?

Nope, but will get you a lot closer to [getting one](/occupations/) --- especially if you put a lot of time into creating high quality projects that establish trust with a potential employer. Every job is different and how you go about creating that trust differs as well. These skills go a long way to creating trust for a wide-range of occupations. Those who put in the intense amount of time and work can ready themselves to move into occupations requiring higher specialization later. *Some* may find junior and internship opportunities without much more work, but the *emphasis and expectation* is that everyone will need at least the same amount of work in addition to the bootcamp to provide lots of proof to a potential employer that they can be trusted to do the job well.

## Will I learn data structures and algorithms?

Yes and no. A proper data structures and algorithms course would probably take 16 weeks just by itself. While you should eventually learn them, strictly speaking they are *not* required for many of the [targeted occupations](/occupations/) (even though many put them into coding interviews). You *will* learn the basic data structures that are a part of all code but won't be focusing on how you would implement them yourself, say in C. Same with algorithms. All code involves algorithms but you will not be isolating specific algorithms and be comparing performance of them. 

## What about the code? Can I use it in my own projects?

Yes without worry. All code in the content is released into the public domain for use however anyone sees fit --- even without attribution.

## Can I run my own beginner boost?

Absolutely! All this content is released under Creative Commons, Share-Alike Attribution so that you can use it and build on it as you like.

## Does this have anything to do with Boostnote?

No. [Boostnote](https://medium.com/boostnote/what-is-boostnote-mobile-5551002869b8) is bloated, Electron note-taking app. Use [Pandoc Markdown](/pandocmark/) in [Git](https://duck.com/lite?q=Git) repos instead.

## Where can I get the source of this?

It's hosted on [GitLab](https://gitlab.com/rwx.gg/README).

