---
Title: Linux File Hierarchy
Subtitle: LFH, Everything is Just a File
tpl-hdsearch: true
---

Everything is a file in Unix/Linux, even your mouse, screen, keyboard, and running [processes](/process/). All of those files and directories are organized in a standard way for all Linux and most Unix systems. The standard is called the *Linux file Hierarchy* (LFH). Understanding it will keep you out of places you might not want to be and help you potentially find people hiding there.

Your Home:

* `repos`
* `go`
* `bin`
* `.config`
* `.local`

Commonly Accessed:

* `/home`
* `/tmp`
* `/media`
* `/mnt`
* `/etc`
* `/var`

Where Executables Live:

* `/bin`
* `/sbin`
* `/usr/bin`
* `/usr/sbin`

Binary Libraries

* `/lib`
* `/usr/lib`

For Application Use:

* `/opt`

Not Really Files:

* `/proc`
* `/dev`
