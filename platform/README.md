---
Title: Platform
tpl-hdsearch: true
---

In tech speak, a *platform* is a [computing environment](https://duck.com/lite?q=computing environment) in which [software applications](https://duck.com/lite?q=software applications) are installed and [executed](/executed/).
