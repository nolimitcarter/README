---
Title: Target Occupations
---

When deciding what topics to include the following target occupations have been considered. These include the fastest growing tech careers according to the [United States Bureau of Labor and Statistics](https://www.bls.gov/ooh/computer-and-information-technology/home.htm):

 Title                                                      Growth   Salary        Schooling   Years
---------------------------------------------------------  -------- -----------  ------------ --------
  [Security Analyst](/secops/)                               32%     \$99,730     Bachelors      4
  [Software Developer](/dev/)                                21%    \$105,950     Bachelors      4
  [Web Developer](/webdev)                                   13%     \$73,760     Associates     2
  [System Administrator](/sysadmin/)                          5%     \$83,510     Bachelors      4 
  [Site Reliability Engineer](/sre/)                         ???       ???        Bachelors      4
  [IoT Embedded Developer](/iotdev/)                         ???       ???        Bachelors      4

:::co-warning
Note the omission of [Computer and Information Research Scientists](https://www.bls.gov/ooh/computer-and-information-technology/computer-and-information-research-scientists.htm) from this list. It is the closest to "data science" and machine learning engineer in the BLS list. It is not included because it  requires a full Computer Science degree with an additional Master's degree even though it commands a *median* salary of \$122,840 dollars per year. The skills learned would still benefit someone seeking such an occupation, but many would not be required. Therefore, the title is not included above.
:::

:::co-contrib
Contributors please keep the above list in mind when considering ideas for additional content. If you cannot confidently answer yes to the question "Does this content apply to every one of these occupations?" then it likely doesn't belong.
:::

::: co-fyi
Incidently, learning web tech (HTML, CSS, JavaScript, DOM, HTTP) is required to become a cybersecurity professional since the web is second only to phishing scams as a hacker attack vector. This is why WGU requires certification as a "Web Site Developer" for their [Cybersecurity and Information Assurance Bachelor's degree program](https://www.wgu.edu/online-it-degrees/cybersecurity-information-assurance-bachelors-program.html)). It goes without saying that deep web skills are also required to win most bug bounties.
:::

## Resources

[Indeed: Job Searching](https://indeed.com/){.see}
[Verified Information on Tech Salaries](https://www.levels.fyi/){.see}
