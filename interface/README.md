---
Title: Interface
Subtitle: Connecting Two Things
tpl-hdsearch: true
---

An *interface* is a way to connect or interact with something. For example, a power plug uses a power interface to plug in. Interfaces usually comply with some sort of standard so anything made to use the interface works, like being able to plug your phone into your computer. This same term is used to connect [computer users](https://duck.com/lite?q=computer users) to their computers and devices. These are called [user interfaces](/ui/).
