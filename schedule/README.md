---
Title: Course Schedule
Subtitle: When and What
---

Content is organized by weeks. Current week is highlighted. Topics and skills are included that are required by *all* [target occupations](/occupations/). As with everything in this [knowledge app](/pka/) the schedule and content will change frequently. Check back often.

1. [Getting Started](/wk01/)
1. [Grokking, Installing, and Running Linux](/wk02/)
1. [The Linux Command Line, I-III](/wk03/){.current-week}
1. The Linux Command Line, IV Bash Scripts
1. Structured Data and Knowledge Source
1. Eloquent JavaScript, Chapters 1-7
1. Eloquent JavaScript, Chapters 8-14
1. Eloquent JavaScript, Chapters 15-21
1. Learning Web Design, I,V HTML/Images
1. Learning Web Design, II CSS
1. Head First C, Chapters 1-4
1. Head First C, Chapters 5-8
1. Head First C, Chapters 9-12
1. Head First Go, Chapters 1-5
1. Head First Go, Chapters 6-12
1. Head First Go, Chapters 13-16

<style>
.current-week {
  background-color: darkgrey;
}
</style>

## Calendar

              May 2020                 June 2020      
        Su Mo Tu We Th Fr Sa      Su Mo Tu We Th Fr Sa
                               #5     1  2  3  4  5  6
     #1     4  5  6  7  8  9   #6  7  8  9 10 11 12 13
     #2 10 11 12 13 14 15 16   #7 14 15 16 17 18 19 20
     #3 17 18 19 20 21 22 23   #8 21 22 23 24 25 26 27
     #4 24 25 26 27 28 29 30   #9 28 29 30
     #5 31
               July                 August       
        Su Mo Tu We Th Fr Sa      Su Mo Tu We Th Fr Sa
     #9           1  2  3  4  #13                    1
    #10  5  6  7  8  9 10 11  #14  2  3  4  5  6  7  8
    #11 12 13 14 15 16 17 18  #15  9 10 11 12 13 14 15
    #12 19 20 21 22 23 24 25  #16 16 17 18 19 20 21 22
    #13 26 27 28 29 30 31

##  Annual Schedule

 Start         Weeks   Content
------------- -------  -------------------------------------
 May  4, 2020   16      **Linux Beginner Boost**
 Aug 23, 2020    1      (Break)
 Aug 30, 2020   16      **Linux Beginner Boost**
 Dec 21, 2020    2      (Break)
 Jan  5, 2021   16      **Linux Beginner Boost**
 Apr 26, 2021    1      (Break)
 May  3, 2021   16      **Linux Beginner Boost**

## What's Next?

This boost is just to get you started right (and as soon as possible) but you aren't *nearly* done with the immense amount of learning required to work in any of the [target fields](/occupations/). Plan on spending an additional 3000-5000 hours of learning to be able to be a competitive candidate for any of the target occupations.

:::co-tip
Plan to go to college. The statistics still show that college graduates --- no matter what the college --- regularly make 30% more *at every stage of their careers*. Every case is different and candidates can be hired without a degree, in fact, several Silicon Valley companies have written an affidavit stating that they do *not* require degrees, only demonstrateable skills and knowledge. Still, the safe bet is to get a degree, perhaps from [WGU](https://wgu.edu) or a local community college that does not put you into massive debt. The studies show the degree doesn't matter, only that you have one. It is definitely unfair, but that's just the way it is, for now.
:::

So what comes next? That really depends, but here are the most ubiquitous languages and technologies that didn't make that cut to fit into a 16-week intensive boost (in order of relative, broad value to learn):

   Languages                  Technologies
---------------- -------------------------------------------
 Python                     Databases and SQL
 PowerShell                 GraphQL
 Rust                       Web Framework (Vue)
 C#                         Docker
 C++                        Networking 
 Erlang                     Data Structs and Algorithms
 Java                       Machine Learning
 Assembly                   Embedded Electonics

These will rank in importance differently depending on the occupation you are considering.

## Why JavaScript Before HTML?

Because it is more important to learn another [imperative language](/languages/) immediately after Bash than a [markup](/markup/) language. Besides the limited exposure to the DOM and HTTP while learning JavaScript leads nicely into HTML after that creating a motivation and justification to learn HTML in order to create [progressive web applications](/pwa/).
