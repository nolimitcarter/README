---
Title: Emacs
Subtitle: More Than Just a Text Editor
tpl-hdsearch: true
---

Calling *Emacs* a "text editor" is almost insulting since it does so much more. Put simply, Emacs fits the definition of a terminal [IDE](https://duck.com/lite?q=IDE) far better than text editor. People love to pick on Emacs, but here's the reality. Emacs is the test terminal developer environment every made and it was created exactly to be that. The priorities of the GNU team who built it were writing *lots* of C code to bring us [GNU](https://duck.com/lite?q=GNU) the most significant set of tools that is still in use today combined with the [Linux kernel](https://duck.com/lite?q=Linux kernel).

We don't use Emacs in the course but if you are likely to take an occupation where you prefer the terminal and just want to get a lot done without leaving it *and* you never need to log into other Unix systems and work on them very much at all, well, then maybe Emacs is for you. By the way this describes a great many academics which is likely why your college professor uses (and suggests you use) Emacs. Make your own decision.

:::co-mad
So next time you want to fight about whether Vi is better than Emacs don't. You are just showing how stupid you are. They are *entirely* different tools. It's like fighting over using a multi-purpose shop tool bench vs a skill saw. They do different things and have different scopes.
:::
