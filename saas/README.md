---
Title: Software as a Service / SaaS
tpl-hdsearch: true
---

When you use a web app to read your email, schedule a meeting, or even watch a movie you are using *software as a service*. You don't install anything, you just use if over the Internet in the "cloud" as people say.

[Cloud Services](/cloud/){.see}
