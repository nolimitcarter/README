---
Title: Virtualization / Virtual Machines
Subtitle: A Machine Within a Machine
tpl-hdsearch: true
---

*Virtualization* is simulating the real thing. [IBM invented](https://blogs.technet.microsoft.com/chenley/2011/02/08/who-invented-virtualization/) computer virtualisation and VMWare later popularized it into the main stream. A *virtual machine* is a computer (machine) running *inside* of another computer operating system. The VM is called a *guest* and the host is, well, the *host*. VirtualBox is the most popular virtual machine software since it is free and available for all major desktop operating systems. VMWare is also available and is more stable, but has a \$300+ price tag. Other options exist for Linux including Qemu and KVM.

:::co-warn
[Containerization](/contain/) is often confused with virtualization. Be sure to know the difference.
:::

## Resources

[Who Invented Virtualization?](https://blogs.technet.microsoft.com/chenley/2011/02/08/who-invented-virtualization/)
