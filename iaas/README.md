---
Title: Infrastructure as a Service / IaaS
tpl-hdsearch: true
---

*IaaS* is [infrastructure](/infra/) provided as a service allowing it to be turned up or down depending on varying needs at different times. Usually this is what people mean when they say "cloud" services (although that is [not all](/cloud/)). Amazon (AWS), Microsoft (Azure), Google (Compute Engine), Digital Ocean, and Linode are all examples of IaaS providers.

[Cloud Services](/cloud/){.see}
