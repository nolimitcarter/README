---
Title: The Linux Command Line, I-III 
Subtitle: Week Three
---

* [Day 11]
* [Day 12]
* [Day 13]
* [Day 14]
* [Day 15]

## Day 11

*Read and practice everything through page 127*

### Grok Annotations Approach

* The [W](/w/) in [RWX](/rwx/)
* [Books](/books/) Still Matter
* Build on Work of Others
* Opinions are Good

### Logistics

* How to view a PDF
* Overview of Week

### Learning Project Ideas

* <https://OverTheWire.org/wargames/bandit>
* <https://exercism.io>
* <https://typing.com>

### Part 1: Learning the Shell

* What is the Shell?
* Navigation
* Exploring the System
* Manipulating Files and Directories
* Working with Commands

## Day 12

*Read and practice everything through Page 170*

### Part 1: Learning the Shell (Continued)

* Redirection
* Seeing the World as the Shell Sees It
* Advanced Keyboard Tricks (`set -o vi`)
* Permissions 
    * Inodes and types
* Processes

## Day 13

*Read and practice everything through Page 161*

### Part 2: Configuration and the Environment

* The Environment
* A Gentle Introduction to `vi`
* Customizing the Prompt

## Day 14

*Read and practice everything through Page 250*

### Part 3: Common Tasks and Essential Tools

* Package Management
* Storage Media
* Networking
* Searching for Files
* Archiving and Backup

## Day 15

*Read and practice everything through Page 362*

### Part 3: Common Tasks and Essential Tools (Continued)

* Regular Expressions
* Text Processing
* Formatting Output
* Printing
* Compiling Programs
