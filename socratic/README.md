---
Title: Socratic Method
Subtitle: What's a Question?
tpl-hdsearch: true
---

The *Socratic method* (named after Socrates) is a form of dialog involving questions and answers designed to promote discussion and stimulate critical thinking to draw out ideas and underlying presuppositions.

