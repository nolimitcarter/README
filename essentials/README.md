---
Title: Essential Tools and Services
---

Here are the essential tools and services you should master (or at least become familiar with) if you intend to work in the tech industry today. 

:::co-remember
Keep in mind that a lot of people working in the tech industry today have no idea what a lot of these are. You will.
:::

## Applications

In the application tools category there are industry tools that might be more common (such as Adobe Photoshop) but this list provides only free alternatives.

---------------------  ------------------------
             Category  Suggested
---------------------  ------------------------
Graphic Web Browser    Brave or Chromium

Passwords              KeePassXC

Graphic Editor         VSCodium

Text Web Browser       Lynx

Virtual Machines       VirtualBox or VMware 

Containers             Docker

Terminal Editor        Vi (Vim)

IRC                    HexChat or WeeChat

Knowledge Source       Pandoc

Terminal Shell         Bash 4+

SpreadSheets           LibreOffice

Presentations          LibreOffice

PDF Viewer             [MuPDF](/mupdf/)

Raster Images          Glimpse

Vector Images          Inkscape

Audio Processing       Audacity

Web Site Link Checks   Muffet

Command Line JSON      `jq`

Web Dev Previewing     Browsersync

Web Dev Tools          Chromium DevTools
---------------------  ------------------------

## Services

[IT Cloud Services](/services/){.see .center}

---------------------  ------------------
             Category  Suggested
---------------------  -------------------
Email                  ProtonMail.com

Rich Chat              Discord.gg

Dev Collab             GitHub.com

Source Hosting         GitLab.com

Web Hosting            Netlify.com

DNS Hosting            NameCheap.com

Cloud Servers          Linode.com or DigitalOcean.com

Twitter                Twitter.com

Linkedin               LinkedIn.com

VPN                    ProtonVPN.com

Diagrams               Draw.io
---------------------  -------------------

## Justification

These selections are based on what meets the demand best based on the requirements of the industry, not personal preferences. For example, Chrome and Chromium based web browsers are now the standard and possess the most useful web application developer tools --- especially when dealing with web site performance analysis and progressive web app service worker development.

