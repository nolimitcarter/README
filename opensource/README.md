---
Title: Open Source
Subtitle: Not Same as *Free* Software
tpl-hdsearch: true
---

*Open source* originally meant that the source code of software was made available openly to everyone under terms much different than those of proprietary software. Eric S Raymond's book, The *Cathedral and the Bazaar* popularized the term which has come to apply to much more than just software today. The scope of open source is much more permissive than [Free Software](/fsf/) since it allows the source to be used in ways that do not require changes made to the original software to be given back.

## Resources

[What is Open Source?](https://opensource.com/resources/what-open-source){.see}

