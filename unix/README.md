---
Title: Unix
tpl-hdsearch: true
---

*Unix* is the ancestor of [Linux](https://duck.com/lite?q=Linux). It was created at AT&T Bell labs by Ken Thompson (one of the three creators of the [Go programming language](https://duck.com/lite?q=Go programming language), Dennis Ritchie, and others. Originally it was coded in [Assembly language](https://duck.com/lite?q=Assembly language) language but was later rewritten entirely in a new language created specifically to create UNIX from scratch in a higher-level language than Assembly, the [C](/c/) language.

Many other operating systems have been derived from the original AT&T Unix including the following:

------------ -------------------------------------------------------------------
 FreeBSD     "Stolen" by Apple (now Darwin in macOS)
 AIX         IBM's proprietary version
 IRIX        From Silicon Graphics and Jurrasic Park ("It's a UNIX system.")
 Minix       By Andrew S. Tanenbaum, Linus Torvaldz instructor inspiring Linux
 SunOS       Later Solaris by Sun Microsystems (now Oracle)
 SCO         (Don't even ask just be glad it's dead and gone.)
------------ -------------------------------------------------------------------

:::co-tip
Depending on your career and industry focus you may very well end up having to edit a configuration file remotely on one of these versions of Unix, which is why you should *never* learn [Vimisms](/vimisms/) that are not supported by just plain old [vi](/vi/).
:::


