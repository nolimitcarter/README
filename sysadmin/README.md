---
Title: System Administrator
Subtitle: Bastard Operators from Hell
tpl-hdsearch: true
---

A *System Administrator* keeps the computers up and operational. This [occupation](/occupations/) isn't surging in growth but also isn't slowing. There are still servers to be maintained even in a "#serverless" world. Most sysadmins will specialize in a particular operating system family such as Linux, Windows, or Mac. Google famously decided to start calling their sysadmins [Site Reliability Engineers](/sre/) instead. 

:::co-meh
The title *System Administrator* has really fallen out of favor with the tech industry and culture. It has become associated with the very few sysadmins who never learned to code or do anything other than swap out RAID 5 hard drives that went bad on servers in the computer room. 

Times have changed but the needs haven't. Good sysadmins have *always* been coding up a storm creating proactively planning for and preparing for system failures with automations and monitor scripts for all sorts of things. Giving them a new title just de-emphasizes that the old, busted approach of sitting on your ass until something breaks and never learning anything new. No wonder they were always grumpy.
:::
