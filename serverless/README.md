---
Title: Serverless
Subtitle: They're Still There, Just Not Your Problem
tpl-hdsearch: true
---

*Serverless* is the common way to refer to putting your web application entirely on a [platform](/paas/) where maintaining and securing the servers is someone else's responsibility and not yours. In addition, any backend logic and processing is handled by services also providing backend [functions](/faas/) that do not require even a web server to use.

:::co-yea
The serverless movement has really increased the amount of raw power a web developer can leverage since such developers are no longer burdened with the hassle of all the infrastructure and can focus *entirely* on making great applications.
:::
