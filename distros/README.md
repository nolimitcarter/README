---
Title: Linux Distributions
Subtitle: The Many Flavors of Linux
tpl-hdsearch: true
---

A *Linux distribution* or *distro* is a Linux operating system put together with a specific purpose or style. Usually most distros today are distinguished by the [package manager](/pkg/). This course is focus primarily on the Debian-based distributions which include [PopOS](/popos/), [Linux Mint](https://duck.com/lite?q=Linux Mint), and [Ubuntu](https://duck.com/lite?q=Ubuntu). These distros are usually easier for first-time Linux users to set up and maintain. They are also more widely supported by hardware manufacturers such as those who make graphics cards and produce games. Stream, for example, only officially supports Ubuntu (although people have gotten it to work with many others).

[Linux Distribution Timeline (local)](Linux_Distribution_Timeline.svg){.see}
[Linux Distribution Timeline (remote)](https://upload.wikimedia.org/wikipedia/commons/1/1b/Linux_Distribution_Timeline.svg){.see}


## Debian

[Debian](https://duck.com/lite?q=Debian) is currently the mother of most other popular Linux distributions today and includes [Ubuntu](https://duck.com/lite?q=Ubuntu) (the most popular Debian-based distro), [Linux Mint](https://duck.com/lite?q=Linux Mint), [PopOS](/popos/), and dozens of others.

## Arch

When the time is right you should explore other distros as well, notably [Vanilla Arch](https://duck.com/lite?q=Vanilla Arch) to learn the inner workings of Linux as you do the installation. Right now it is the best option for those who really want to understand Linux while still having a distro with strong software package support. Arch is world-renowned for the Arch User Repository approach to package management, organization, and distribution. Arch is also very much the coolest Linux distro to have once you are ready for it.

For those looking for an Arch that is a bit more built for them [EndeavorOS](https://duck.com/lite?q=EndeavorOS) is the most promising and respected while [Manjaro](https://duck.com/lite?q=Manjaro) is still used by many despite the many disastrous fails the its parent organization and technical team have survived.

:::co-warning
Manjaro is generally a very bad beginning distribution for many reasons chief of which is its relative (and surprising) instability. It may seem good a start but the stories of people having serious problems are well documented. In one case clicking the update button bricked thousands of people's machines and Manjaro's official response was *no* response leaving the forums filled with frustrated beginners who were being condescendingly told they should have run the upgrade from pseudo-tty instead of clicking "Update". Manjaro as an organization is also downright unethical in many of its corporate practices and has repeatedly demonstrated its focus on profit above the needs of its users and community. Obviously people will make their own decisions from their own experiences, but there are *a lot* of blogs and videos from people echoing these experiences and conclusions.
:::

## Gentoo

[Gentoo](https://duck.com/lite?q=Gentoo) is a Linux distro that requires everything to be compiled from source. Make absolutely sure you have a *very* powerful CPU if you go this route or you will be waiting around forever just to get a minimal install running.

## RedHat

You should also get some experience with [Fedora](https://duck.com/lite?q=Fedora) or [CentOS](https://duck.com/lite?q=CentOS) since they are based on [RedHat](https://duck.com/lite?q=RedHat), which is now owned by IBM. In fact, RedHat's CEO is now IBM's President. Even if you hate it understanding how to use and support RedHat systems is a mandatory skill when working for most large enterprises.

## Suse

[Suse](https://duck.com/lite?q=Suse) Linux is *very* close ties with Microsoft and could very well come a very tightly integrated provider of Linux to Azure cloud. No matter what you might think of Microsoft of Azure there is a strong change you will need to know and support it during your career if you choose a Linux-based occupation.

## Raspian

[Raspian](https://duck.com/lite?q=Raspian) is the Linux distribution specifically made for the [Raspberry Pi](https://duck.com/lite?q=Raspberry Pi).

## Kali

[Kali Linux](https://duck.com/lite?q=Kali Linux) is well known in the pentesting community. While there are other pentesting focused distributions it is by far the most well known (and best marketed). While Kali is a good one-size-fits-all security distro most professional pentesters will usually customize their own Arch distros instead only putting the tools they want and need instead of using the very bloated Kali distro. 

## Parrot

[Parrot](https://duck.com/lite?q=Parrot) is another pentesting security distro that is preferred by many over Kali among those who do not mind the bloat from such distros.

:::co-fyi
People often confuse [Linux From Scratch](https://duck.com/lite?q=Linux From Scratch) as a distribution. It's not. It's a book, one that tells you how to make a Linux distribution, well, from scratch. Those working with very small embedded devices will find this useful.
:::
