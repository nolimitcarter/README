---
Title: Change Log
---

Here are the major changes to this [knowledge app](/pka/). Refer to this rather than `git log` since other [smaller changes are made frequently](/knowledge/git/).

##  Wednesday, May 20, 2020, 4:49:06PM

* [Build MuPDF for Dark Mode](/mupdf/)

##  Wednesday, May 20, 2020, 9:56:06AM

* Change log introduction
* [Long term todo list](/todo/#long-term)
* [Git Knowledge](/knowledge/git/)
* [CI/CD of Knowledge Source](/knowledge/cicd)

##  Tuesday, May 19, 2020, 5:02:14PM

* Added *Make a Learning Plan to Day 1*
* [TODOs](/todo/)
* [Early Motivators for Tech Learning](/motivate/)
* [Set Up Local GitLab Project Repos](/localrepos/)
* [Git Host Service Ping](/gitping/)

##  Monday, May 18, 2020, 8:47:23PM

* ***Every single link is fixed!***

Links to things that will eventually be included locally in the [app](/pka/) are currently to DuckDuckGo.com external Internet searches, which serve the primary purpose for now of getting the information to the person learning.

##  Monday, May 18, 2020, 7:20:27PM

* [Style Guide](/styleguide/) updated with default external search link rules
* [Executed](/executed/)
* [Users](/users/)
* [Tron](/films/tron/)

##  Monday, May 18, 2020, 4:59:09PM

* YouTube links directly to playlist
* Added (Back) Discord Link to Menu

##  Monday, May 18, 2020, 10:35:48AM

* [GNU/Linux](/gnulinux/)
* [Linux File System](/lfs/)
* [Linux File Hierarchy](/lfh/)
* [Week Three](/wk03/)

##  Sunday, May 17, 2020, 2:32:58PM

* [Get Vim Ready for Go Programming](/vimgo/)
* [Install Golang](/installgo/)
* [Special `faq` Divs](/styleguide/#special-faq-divs)
* [JAMstack](/jamstack/)
* [Serverless](/serverless/)
* [Static Site Generators](/ssg/)
* [Netlify](/netlify/)

##  Sunday, May 17, 2020, 10:50:17AM

* Lots of new content and fixed links
* [RWX](/rwx/) link added to [Overview](/overview/)
* [TLCL](/books/tlcl/) annotations started
* [`check`](/check) script added for link validation
* [Corruption Abounds and Destroys Education](/corrupt/)
* Link to [Not Wiki](/wikis/) from [MVKB](/mvkb/)

##  Saturday, May 16, 2020, 2:10:32PM

* Dropped [Streamers](/streamers/) from Main Menu
* Moved [FAQ](/faq/) link from Main Menu to [Overview](/overview/)
* [For Loops with in Versus of](/books/eloqjs/#for-loops-with-in-versus-of)

##  Saturday, May 16, 2020, 8:32:37AM

* `tpl-` YAML prefixes
* Bug preventing main page build
* [Annual Schedule](/schedule/)
* [What's Next?](/schedule/#whats-next)
* Videos in [weekly schedule](/wk02/)
* Job search resources in [occupations](/occupations/)

##  Friday, May 15, 2020, 7:19:31PM

* [Set Up Secure Shell Server](/setupsshd/)
* [Authorize a Remote Public Key](/authpubkey/)
* [Daemon](/daemon/)

##  Friday, May 15, 2020, 9:37:26AM

* Removed *Creating Command Line Utilities in Go* from week 5
* Added back extra week of *Head First Go*
* [Syntax](/syntax/)
* [Whitespace](/whitespace/)
