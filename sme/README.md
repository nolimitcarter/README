---
Title: Subject Matter Expert / SME
tpl-hdsearch: true
---

A *subject matter expert* (SME) is one who is a expert on a subject. This term is used as a synonym for *specialist* in the industry. (God knows we love acronyms where a plain noun would do just fine.) An SME is regularly involved in the technical vetting process when interviewing new applicants for positions or new contracts, etc.

:::co-tip
As your technical career grows you will increasingly be called upon to serve as the SME in different meetings and interviews, so much so that if you are not careful you will get sucked out of your technical role and into more of a managerial or project leadership position. This is fine if that is your intent. In fact, it usually pays *way* better. But if you want to keep your technical skills fresh and remain in a purely technical position this can be devastating. 

Before you know it you will be promoted out of your technical position and replaced with someone who spends all day, every day doing nothing but technical stuff you so dearly love. Suddenly you are too important to do the stuff that brought you into this career in the first place. Hungry technologists know that there isn't a second to spare in this fast-moving technical industry if you want to stay on top of your technical game, not even for that flattering, boring, and skill-dulling meetings with executives where you are the big kahuna in the room. The feeling can be *very* addicting but may take you away from your long-term goals. Just be aware of *manager suck* and balance your focus appropriately.
:::
