---
Title: Early Motivators for Tech Learning
Subtitle: Keeping It Fun
tpl-hdsearch: true
---

Having a specific thing in mind makes learning the boring stuff easier because you know what you are *eventually* going to get out of it. People are motivated by different things. Here are a few examples of project goals that tend to motivate early-stage technologists. Consider picking one and [writing](/w/) it down as part of your [learning plan](https://duck.com/lite?q=learning plan).

* Set Up a Minecraft Server
* Develop a Game
* Create a Web Site
* Hack and Defend (CTF)
* Get a Job


