---
Title: Graphical User Interface
Subtitle: GUI
tpl-hdsearch: true
---

A *graphical user interface* or *GUI* is an [interface](/interface/) that allows you to control something, usually a computer or device. GUIs have become standard for most all computers but [servers](/server/) frequently do not have one requiring proficiency with a [terminal user interface](https://duck.com/lite?q=terminal user interface) or [*command line interface*](/cli/).
