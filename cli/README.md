---
Title: Command Line Interface
Subtitle: One Line of Input, One or More Lines of Output
tpl-hdsearch: true
---

A *command line interface* or *cli* takes in one line of [standard input](https://duck.com/lite?q=standard input) from a programming running in a [REPL](/repl/), evaluates the input, and responds by printing out one or more lines to [standard output](https://duck.com/lite?q=standard output) or [standard error](https://duck.com/lite?q=standard error) both of which appear to be separate lines on the [terminal](/terminal/).
