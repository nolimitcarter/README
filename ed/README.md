---
Title: Ed Command Line Editor
Subtitle: World's First Major Text Editor
tpl-hdsearch: true
---

*Ed* is the only [*command line*](/cli/) editor (as opposed to a [*terminal*](/terminal/) editor like [Vi](/vi/)) still is major use. It is still alive and well and used by discerning Pentesters because it is almost always installed on any Unix system and works without a full visual terminal environment (which Vi requires). 

:::co-comment
Although we won't learn Ed as a part of the course you should strongly consider learning it if for nothing  more than bragging rights. It is *way* cooler to learn Ed than [VSCode](/vscode/).
:::
