---
Title: Not Like Other Courses
---

In many ways this course is exactly the opposite of courses you have had in the past:

* No assignments, only suggestions and collaboration
* No lessons, just explanation and discussion
* No you versus test or others, just you versus you
* No grades, only finished projects that *you* decide
* No teachers, just experienced people here to help
* No classmates, but a community of learners
* No jobs, just confidence and trust you can foster

If you are uncomfortable with this level of personal responsibility and expectation then this course might not be for you. Either way know that you are among friends, not competitors. We are *all* learning something and having something to help others learn. Some of us just got started earlier.
