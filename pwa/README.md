---
Title: Progressive Web Apps / PWA
tpl-hdsearch: true
---

A *progressive web app* (PWA) is a web application that behaves like a native mobile or desktop application but without the hassle of any [app store](/appstores/).
