---
Title: Knowledge as Source
Subtitle: Capturing, Maintaining, and Sharing Knowledge as Source Code
tpl-hdsearch: true
---

We live in a time when the amount of human knowledge has reach unprecedented levels and grows exponentially, but we are at risk. So much knowledge is being produced in different forms that strategies for maintaining and archiving it in a way that is not owned by proprietary interests have been all but ignored. By applying the best practices and lessons learned from managing and distributing opensource software source code to *knowledge* source we can leverage all the same successes these projects have enjoyed. This begins by capturing the knowledge is a [simple form](/basicmark/) such that source management and version control tools [can work with it](git/) and then building building [knowledge applications](/pka/) from the that source.
