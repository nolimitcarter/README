---
Title: Git for Knowledge Source Management
Subtitle: Commit Often, Don't Branch, Log Changes
tpl-hdsearch: true
---

[Git](https://duck.com/lite?q=Git) is the world's best tool for managing source code and is therefore the best for managing knowledge captured as [Pandoc Markdown](/pandocmark) source. However, much of Git's functionality does not apply to the much more dynamic work of maintaining and editing knowledge source. Knowledge source is also much less able to benefit from the same level of fully automated [CI/CD](/knowledge/cicd/) because of this and the essential need for *human* editors to do what might otherwise be automated unit testing. Here are a few guidelines to get started:

* Commit often
* Create a `save` command
* Don't bother branching
* Work entirely on `master` branch
* Log changes to a [changes](/changes/) [knode](/knode/)
* Consider a different group just for knowledge source to prevent flooding those watching your Git activity on your hosting service.

