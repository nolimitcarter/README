---
Title: Platform as a Service / PaaS
tpl-hdsearch: true
---

When you use a service to host your application you are using *[platform](/platform/) as a service*. Google App Engine is one example of such a service.

[Cloud Services](/cloud/){.see}
