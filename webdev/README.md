---
Title: Web Developer / Full Stack Developer
Subtitle: 'What even is a *Full Stack Developer*?'
tpl-hdsearch: true
---

*Web developer* (aka [Full Stack Developer]) is the third fastest growing [technology occupation](/occupations/) and requires the least amount of education.

:::co-warning
While there are no stats to back it up, it seems from all the social media posts in 2019 and early 2020 that --- despite the [BLS stats](/occupations/) --- the industry is glutted with Junior Web Developers right now looking for work. Perhaps from all the Bootcamps. If this is true the answer is to make yourself *senior* from the beginning by doing more challenging projects.
:::

## Full Stack Developer

A *Full Stack Developer* is just a Web Developer who claims to be able to do both front-end development (all the stuff that happens in the web browser) *and* all the back-end development (making APIs, database queries, setting up web services, cloud integration, etc.). 

:::co-mad
While it is true that individuals exist who have truly mastered everything to give themselves this moniker, the truth is that this idiotic title triggers most of the rest of the tech industry because of its insufferable ambiguity and ridiculous claim that a single person can be equally proficient at the *vast* amount of technologies requiring highly specialized skill. Mastering database administration, for example,  is a highly sought after advanced skill by itself commanding median salaries in the \$150k range. When interviewed most "Full Stack Developers" can't even form a basic SQL join to create a view. Just be aware of this cultural minefield and be careful if you choose to call yourself by this title. Web developers get away claiming they know *everything* on the front end and back because of technologies like Ruby on Rails that have largely insulated them from the specifics of the technologies they claim to understand with this blank title. (Knowing migrations does not mean you know databases.) 

Just be prepared for the additional vetting you will be receive from an astute [SME](/sme/) if you choose to put this title on your resume. 
:::

## Resources

* [Learning Web Design](/books/lwd/)
