---
Title: App Stores
Subtitle: What the F*** Were We Thinking?!
tpl-hdsearch: true
---

> There are 2 billion web users versus 50 million iOS users. (Brian Kennish, former Google engineer turned privacy advocate)

*App stores* used to be where you had to get your apps. They were a *massively failed attempt* to monopolize the ecosystem for everything that runs on *any* device --- even desktop and laptop computers. Thank God smart, informed, ethical people have promoted standards-based alternatives such as technologies like [progressive web apps](/pwa/) instead that have flourished while app stores dwindle and die like shopping malls during the 2020 COVID crisis.

:::co-dumb
The idea that three or four companies would entirely control the vast majority of all software distribution through a crappy, poorly-vetted, highly biased, 30% extorted "market" is downright idiotic. One day people in the future will look back at how we collectively lost out minds to allow this to happen.
:::

## Resources

[Why Mobile Apps Will Soon Be Dead (2011)](https://www.technologyreview.com/2011/05/19/194615/why-mobile-apps-will-soon-be-dead/){.see}
[Why Progressive Web Apps Will Replace Native Mobile Apps](https://www.forbes.com/sites/forbestechcouncil/2018/03/09/why-progressive-web-apps-will-replace-native-mobile-apps/){.see}
