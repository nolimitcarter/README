---
Title: Pandoc Markdown
Subtitle: World's Best Syntax for Knowledge Source
tpl-hdsearch: true
---

:::co-warning
If you haven't mastered [Basic Markdown](/basicmark/) yet start there.
:::

[Pandoc](/pandoc/) has been the academic standard for capturing knowledge source in the form of textbooks, web sites, slides, ePubs, and more for more than a decade. It is the *only* widely adopted [Markdown](/md/) that fully support LaTeX, the language of mathematics. As such, it is critically important that everyone learn it who wishes to capture, manage, and share their knowledge for whatever purpose.

:::co-fyi
The R language project liked Pandoc Markdown so much it adopted it as the standard documentation format for the language itself.
:::

## Resources

* [Pandoc Markdown Official Documentation](https://pandoc.org/MANUAL.html#pandocs-markdown)
