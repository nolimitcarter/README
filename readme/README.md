---
Title: A README File
tpl-hdsearch: true
---

A README file is a long-standing tradition in the tech world for including a file that contains the most critical information for getting up and running with a thing. These days the exact file name is almost always `README.md` since [Markdown](/md) has dominated the world of [knowledge source](/knowledge/).
