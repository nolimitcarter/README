---
Title: Markup Language
tpl-hdsearch: true
---

Markup is writing that is marked up with formatting symbols and syntax. [HTML](/html/) and [Markdown](/md/) are examples of markup languages.
