---
Title: Spaghetti Code Kills
Subtitle: Proven In Court By a NASA Engineer
tpl-hdsearch: true
---

> "I just need to add this one thing for now."  

*Spaghetti code* is [technical debt](https://duck.com/lite?q=technical debt) that has accrued within a [code base](https://duck.com/lite?q=code base) from [procrastinating](https://duck.com/lite?q=procrastination) proper [refactoring](https://duck.com/lite?q=refactoring code) and testing.

