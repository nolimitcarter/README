---
Title: Tron
Subtitle: First Major Motion Picture to Contain CGI
tpl-hdsearch: true
---

A revolutionary film made in the 80s containing the first fully computer generated character and many other 3D animation entertainment hallmarks. The language and terms from Tron remain indelibly burned into collective tech pop culture and have made showings as recently as references in [Ready Player One](https://duck.com/lite?q=Ready Player One).

![Film Poster for Original Tron](tron.jpg)
