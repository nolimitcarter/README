---
Title: Getting Started
Subtitle: Week One
---

* [Day 1]
* [Day 2]
* [Day 3]
* [Day 4]
* [Day 5]

## Day 1

[📺]{.noinvert} [Linux Beginner Boost - Day 1 (May 4, 2020)](https://youtu.be/CI-FE2bKr7c)

:::co-fun
May the Fourth be with you, indeed.
:::

### Welcome

* Get to Know Each Other

### Course Stuff

* Grok [Grok](/grok/)
* Grok [Course Overview](/overview/)
* Grok [Course Goals](/goals/)
* Grok [Open Credentials](/cred/)
* Grok [Course Logistics](/logistics/)
* Grok [Course Rules](/rules/)

### Tools and Services

* Review [Essential Tools and Services](/essentials/)
* Set Up [Brave](https://brave.com) or [Chromium](https://www.chromium.org)
* Set Up [Discord](https://discord.gg)
* Set Up [Protonmail](https://protonmail.com)
* Grok [REPL.it](https://repl.it)

### Write Some Code

* Play with [Course Languages](/languages/#recommended)

:::todo

-----------------------------------------------------------------------
Learning Project Ideas
-----------------------------------------------------------------------
Write a Hello World program in every course language and others.

Copy your Hello World programs to your notes inside of code fences.

Research the pros and cons of Brave vs Chromium vs Chrome.

Read all the ProtonMail support pages.

Customize your ProtonMail web interface.

Go for a walk and ponder what you *like* to do and write it down.

Ponder what people tell you that you are good at and write it down.

Imagine what your best work day would be like and write it down.

Consider if and how technologies covered match your personal goals.

Post a few messages to the community Discord channel.
-----------------------------------------------------------------------

:::

## Day 2

[📺]{.noinvert} [Linux Beginner Boost - Day 2 (May 5, 2020)](https://youtu.be/Dy4AVASFT_A)

### Using Your Computer

* Grok Origins of Computer Input and Output
* Know Your [Keyboard](/typing/)
* Know Your Mouse

### Storing Your Code Safely

* Grok Source Hosting Services
* Set Up [GitLab](/gitlab/) 
* Set Up [GitHub](/github/)

### Taking Sustainable Notes

* Take Notes in [Basic Markdown](/basicmark/) on GitLab

### First Web Page

* Identify Good [Web Development Resources](/webdev/#resources)
* Create First Web Page with GitLab Editor
* Set Up [Netlify](/netlify/) with GitLab
* Publish First Web Page to [Netlify](/netlify/)

:::todo

-----------------------------------------------------------------------
Learning Project Ideas
-----------------------------------------------------------------------
Start your notes project repo on GitLab.

Create a learning plan with a daily schedule and your goals.

Port your notes from Day 1 into Basic Pandoc Markdown in notes repo.

Use one of everything from Basic Pandoc Markdown in your notes.

Do your own research to compare Pandoc to other tools and formats.

Take some relevant notes from Google docs and add them to notes repo.

Research different services out there to see which use Markdown.

Complete all of Typing.com.

Invite people in Discord to race you on NitroType.

Play some of the listed typing games.

Force yourself to use one-handed mouse cut and pasting.
-----------------------------------------------------------------------

:::

## Day 3

[📺]{.noinvert} [Linux Beginner Boost - Day 3 (May 6, 2020)](https://youtu.be/h9ZV9O4fIt0)

### Get On the Terminal

* Peek at [Learning the Linux Command Line](/books/tlcl/)
* Grok Importance of [Terminal](/terminal/) Mastery
* [Get](/terminal/#get) a Proper Linux Terminal
* Master Cut and Paste with Linux Terminal
* Learn [Shell Survival Skills](/shell/#survive)

### Editing Files from Terminal

* Grok Problem with Nano
* Grok Problem with Emacs
* Learn [Vi Survival Skills](/vi/#survive)
* Review [Vim Learning Resources](/vi/#resources)
* Create a Hello World Script in Bash

### Set Up Command-Line Git and GitLab

* Grok Basic Public Key Cryptography
* Generate Secure Shell Keys with [`ssh-keygen`](/sshkeys/)
* Add Public Key to GitLab Account Settings
* Install and Configure Git
* Grok Basic Git Source Code Management
* Configure Local Git User Settings
* Turn an Existing Directory into New GitLab Project Repo
* Clone an Existing GitLab Project Repo

:::todo

-----------------------------------------------------------------------
Learning Project Ideas
-----------------------------------------------------------------------
Write your thoughts about the terminal in your notes.

Identify your terminal limiters and write them in your notes.

Argue with yourself about when [GUI](/gui/) or [TUI](https://duck.com/lite?q=TUI) is better.

Practice editing files in Nano for a while to compare.

Practice editing files in Emacs for a while to compare.

Complete all seven parts of `vimtutor`.

Read and practice the [Vi Magic Wands](/vi/#magic).

Do some of the other [Vim Resources](/vi/#resources).

Explain what public key cryptography to yourself in your notes
like you are explaining it to someone.

Move your SSH keys and make some more. Repeat until it comes natural.

Research what Git is about and what files it creates on your
computer.

Delete and re-clone a GitLab project a few times to get used to it.

Create several GitLab repos from scratch to memorize the
`git` commands.

Read up on Bash and create a Magic Eight Ball shell script.

Read the first Part of [The Linux Command Line](/books/tlcl/) in advance.

Navigate around your Linux system with the [survival commands](/shell/#survive).

Teach yourself the Ed editor as well as Vim.
-----------------------------------------------------------------------

:::

## Day 4

[📺]{.noinvert} [Linux Beginner Boost - Day 4 (May 7, 2020)](https://youtu.be/xuBiLyCcTzM)

### Configure Vim

* Configure Local Vim User Settings
* Steal Vim Config from Others
* Review What Vim Config Does
* Avoid [Vimisms](/vimisms/) at All Costs
* Grok Why Small `vimrc` is Better

### Set Up Dotfiles Repo

* Grok Dotfiles Tradition
* Grok Repos Directory Organization
* Set Up Dotfiles GitLab Project Repo

### Configure Bash

* Grok Interactive Command Shells
* Grok Why Bash and *Not* Zsh
* Grok Shell Login Scripts
* Grok Symbolic and Hard Links
* Link `~/.bash_aliases` to Bash Config
* Set Up Bash Environment, Aliases, and Functions
* Steal Bash Config from Others
* Review What Bash Config Does
* Set Up Personal Scripts Directory 

### Get Stuff from Web without Browser

* Curl Stuff from the Web
* Curl `ix` Command from Chat

### Configure TMUX

* Grok Terminal Multiplexors
* Set Up and Use TMUX (Like Screen)
* Steal Others TMUX Config
* Review What TMUX Config Does

### No One is Ever Finished Configuring

:::todo

-----------------------------------------------------------------------
Learning Project Ideas
-----------------------------------------------------------------------
Familiarize yourself with `:help` from within Vim.

Snoop through several other dotfiles directories on GitLab and GitHub.

Learn about `Plug` and add your first plugin.

Customize *all* your configurations by stealing from others.

Look for cool stuff to use `curl` with on the Web.

Practice sharing files and code with friends using `ix`.

Read through all of the TMUX configuration and study about it.

Familiarize yourself with `:help` from within Vim.
-----------------------------------------------------------------------

:::

## Day 5

[📺]{.noinvert} [Linux Beginner Boost - Day 5 (May 8, 2020)](https://youtu.be/k4aSa4EpzkI)

### Basic Networking Overview

* Grok How the Internet Works
* Grok How Your Home Network Works
* Scan Your Network and the Internet

### Remote Secure Shell Connections

* Grok Secure Shell
* Review Secure Shell Configuration
* Practice SSH Connection
* Practice SCP File Transfer

### Configure Lynx 

* Grok Terminal Web Browsers
* Set Up and Use Lynx Web Browser
* Steal Lynx Config from Others
* Set Up Command Line Web Searches

### Personal Privacy and Online Safety

* Danger of Doxing
* Drop Google
* Grok Virtual Private Networks
* Set Up Command Line ProtonVPN
* Set Up KeePassXC

### Learning to Learn

* Grok [Learning to Learn](/learning/)
* Grok Dogma is Death
* Always Be Learning 

### Week One Wrap Up

* Review Everything from Week
* Plan Learning Projects
* Have a Great Weekend

:::todo

------------------------------------------------------------------------
Learning Project Ideas
------------------------------------------------------------------------
Describe how *your home network* works to a non-technical friend or
family member. Or just write down you explanation.

Describe how the *Internet* works to a non-technical friend or family
member. Or just write down your explanation.

Create a [PicoCTF](https://picoctf.com) account and connect to it 
with ssh through a VPN.

Connect to [OverTheWire](https://overthewire.org) with ssh over a VPN.

Practice copying files between PicoCTF and/or OverTheWire with `scp`.

Make a plan and set a goal to get off Google completely.

Setup your own KeePassXC database with auto-typing activated.

Add your *private* SSH keys to KeePassXC and activate SSH Agent

Read through the lengthy old Lynx configuration files.

Install and sample other text-only Web browsers compared to Lynx.

Ponder what cognitive biases you might have and how to protect against

Dig deeply into what it means to be an [autodidact](/autodidact/).

Set some specific skill goals to reduce the number of questions you have
to ask that you can research and answer on your own.

Find an opportunity to contradict or disagree with someone without 
being triggered or triggering them.

Practice balancing no-fear, strong disagreement with respect and concern
for the person attacking the information, concept, or tool and not the
person.
------------------------------------------------------------------------

:::
