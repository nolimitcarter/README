---
Title: Netlify
Subtitle: World's Oldest and Largest JAMstack Provider
tpl-hdsearch: true
---

*Netlify* is an "all-in-one platform for automating modern web projects" and includes a wide content delivery network of servers that put your content as close as possible to the people using it, which is important since the number one factor in usability is TTL, time-to-load. The engineers at Netlify coined the term [JAMstack](/jamstack/) and now have regular conferences on the topic of modern web applications design and deployment.

:::co-tip
It takes about 20 seconds from the time of a Git commit to GitLab until it is picked up and available on Netlify. This delay is so little you can almost do live development using just Git commits or even the GitLab Web IDE editor to make changes in real-time.
:::
