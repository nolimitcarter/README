---
Title: User / Users
tpl-hdsearch: true
---

You are a *user* when you use things, usually a computer, device, app, or system in the tech context.

:::co-ff
This term entered tech pop culture when [Tron](/films/tron/) deified users.
:::
