---
Title: Sublime Text Editor
Subtitle: The Minimal Editor That Started It All
tpl-hdsearch: true
---

The [Sublime Text](https://www.sublimetext.com) editor started the revolution in light-weight [GUI](/gui/) code editing tools over heavier [IDEs](https://duck.com/lite?q=IDEs). This let to [Atom](/atom/), which brought us [Electron](/electron/), and eventually [VSCode](/vscode/).
