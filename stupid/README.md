---
Title: Stupid Bad Advice
Subtitle: Just Don't Do These Things Uninformed People Will Tell You
---

There is so much absolutely horrible advice out there that it has become necessary to create this index of it so you can just send people to it instead of fighting over it. Usually people telling you to do this stuff are just trying to help you but are uninformed. *These people aren't stupid*, but their advice definitely is. Usually once they know how bad it is they will correct themselves. If not, well, then yeah they probably are stupid because they care more about their ego and being right than good information. At least you will know.

* [Don't Pick Emacs Just Cuz]
* [Don't Ever Use Zsh]
* [Don't Use Control L to Clear the Screen]
* [Don't Attempt Redirection to Output a File]
* [Don't `cat` Into a Pipe]
* [Don't Pipe Into While Loops]

:::co-fyi
Don't be confused by the titles. It's easier to have the title *counter* the bad advice rather than state it. So it's not a double-negative ("don't don't").
:::

## Don't Pick Emacs Just Cuz

Emacs is used by people who rarely use more than one computer terminal. Vi is used by people who want to be functional on *any* computer with a Unix/Linux terminal. Emacs users are helpless without their bloated, customized, personalized editor on what is likely their bloated, customized, heavy workstation. Expecting to be able to install Emacs on any computer you are required to work on is ridiculously naive. 

Unfortunately a large number of professors and academics recommend Emacs to their students without even pausing to consider how those students will go on to use the editor. Fostering a dependency on a tool that fundamentally handicaps *most* technologists in *most* professions involving a Unix/Linux terminal is downright irresponsible.

It's not that Vi/m is *better* than Emacs. It's a different tool for a different type of user. Recommending *any* tool without pausing to consider the needs of the user --- rather than asserting your ego --- is stupid.

## Don't Ever Use Zsh

Zsh is now the official shell of Apple. It's not the default on anything else. Bash continues to be the official default shell of all Linux systems. Ask yourself, do you want to learn some broken new default shell from a company that removed the `Escape` key and just wants to avoid GPLv3 or default Linux shell used on every single Linux system for decades with *far* more functionality? It's as simple as that. If you answered Apple then you best just leave this site right now and never come back --- especially if the only reason you picked Zsh was to get that ridiculous powerline command prompt (bubye).

Zsh provides no additional value over Bash, period. But Zsh is *very* broken and Bash provides *far more* than Zsh.

* Zsh handicaps you by preventing you from using critical Bash functionality such as exported functions (which is how all Bash completion on every Linux system is created in `/etc/bash_completion.d`). 

* Zsh users use the `cd` functionality without even understanding how to use `$CDPATH` in Bash to achieve the same result. 

* Zsh even promotes incorrect understanding of shell redirection by allowing `< somefile` to effectively `cat somefile` confusing beginners (who have proven it in our chat when taught irresponsibly to do it). 

* Zsh is *not* POSIX compliant as people claim. Use of that broken redirection suggestion is proof of that. If you are going to learn non-POSIX things they should be for the *default* Linux shell, not some broken upstart that can't decide what it wants to be. If you want POSIX use `/bin/dash` instead, which is what `/bin/sh` is linked to on all Debian-based systems.

## Don't Use Control L to Clear the Screen

Never use `Control`+`l` (`^L`) to clear your screen because it is specific to Emacs mode (`set -o emacs`), the default. While this works by default on most Linux and Unix system terminals. This is only the default because the GNU project created Bash and the GNU project invented and uses the horribly bloated and isolated Emacs editor. Almost everyone should immediately add `set -o vi` to their Bash configuration to get Vi mode instead, which does not support `Control`+`L`.

:::co-tip
If you really are that bothered by typing `clear` then add an `alias c=clear` to your Bash config.
:::

:::co-rage
Nothing shows how little you know about the Unix terminal than to recommend this.
:::

## Don't Attempt Redirection to Output a File

```sh
# WRONG!
< somefile
```

This does not work on Bash. It might on Zsh (or some other irrelevant shell) but it doesn't in Bash. Telling people to do this is irresponsible without telling that it just won't work on most shells including the default.

Use `cat` instead.

```sh
cat somefile
```

## Don't `cat` Into a Pipe

```sh
# WRONG!
cat foo | somecmd something`
```

This creates a subprocess unnecessarily.

Use redirection instead.

```sh
somecmd something < foo
```

## Don't Pipe Into While Loops

```sh
# WRONG!
myvar="a value"
declare -i count=0
grep something somefile | while read line; do
  echo "$myvar: $line"
  count+=1
done
echo "Count: $count"
```

```{.out}
a value: Here is something
a value: And something else
a value: One last something
Count: 0
```

This runs while in a subprocess masking the local variables (`count`) so they are not updated.

Use redirection instead, either file descriptor or temporary file, (which both benchmark at about the same speed).

```sh
myvar="a value"
declare -i count=0
while read line; do
  echo "$myvar: $line"
  count+=1
# creates a tmp file descriptor and "pipes" in
done < <(grep something somefile)
echo "Count: $count"
```

```{.out}
a value: Here is something
a value: And something else
a value: One last something
Count: 3
```

```sh
myvar="a value"
declare -i count=0
while read line; do
  echo "$myvar: $line"
  count+=1
# creates a tmp file with the grep output 
done <<< $(grep something somefile)
echo "Count: $count"
```

```{.out}
a value: Here is something
a value: And something else
a value: One last something
Count: 3
```
