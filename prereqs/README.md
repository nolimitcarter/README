---
Title: Prerequisites
Subtitle: Don't Even Try Without These
---

* Be 13 or older (required by law), 16+ suggested
* Be able to read, write, and do math of most 16-year-olds
* Type 30 words per minute from home row
* Have a computer with a keyboard and a mouse
* Able to use a computer and graphical web browser
* Understand basic parts of a computer
    * CPU Microprocessor (Cores)
    * RAM Memory
    * Storage (Disk, SSD, USB Thumbdrives) 
    * Peripherals (Mouse, Keyboard, Screen)
* Have a compatible computer operating system
    * Windows 10 or above
    * Mac Sierra or above
    * Any Linux
    * No Chromebooks

