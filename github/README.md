---
Title: GitHub
Subtitle: World's Largest Git Hosting Service
tpl-hdsearch: true
---

*GitHub* is home to literally billions of lines of code at this point and used by over 10 million members. It is probably the most important service *for* [open source](/opensource/) in the world and yet it isn't open source itself. Every single technologist should have a GitHub account even if you do eventhing on GitLab or another service you likely will need to connect with other free software contributors and projects.

[Choose GitLab](/gitlab/isbest/)]{.see} 

:::co-fyi
Microsoft paid \$7.5 billion to acquire GitHub in 2018.
:::




