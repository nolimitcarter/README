---
Title: Vi/m Text Editor
Subtitle: The World's Most Ubiquitous Editor 
tpl-hdsearch: true
---

Vi was created in 1976 and is the second oldest editor on the planet, which has also made it the most [ubiquitous](/ubiquitous/). The first editor was [Ed](/ed/) (which is still very much worth learning). The letters `vi` actually stand for `visual`, as in, a "visual" version of Ed. Anyone wanting a career involving Linux and UNIX systems should learn Vi even if they prefer something else for larger project development. 

The `vi` command is usually some form of `vim` on most systems today, but not all. Vim ("Vi Improved") is a larger version of a compatible editor with many great additions, some of which [you should avoid](/vimisms/) so as to not lock-in bad muscle-memory when you are on a system that only has `vi` installed (the arrow keys for example). Today the closest thing to the original `vi` is a modern project called [`nvi`](http://www.bostic.com/vi/) which even recreates the bugs of the original. Comparing `nvi` to `vim` is one reliable way to know if what you are learning is a [vimism](/vimisms/) or not.

:::co-faq

## What About NeoVim?

Vim is the standard editor included on *all* Linux systems by default. That should be reason enough. But there is actually no compelling reason to use NeoVim anyway. 

* Use [TMUX](/tmux/) instead of panes. 
* No one cares about the codebase and developer team drama. 
* Vim is more modern than NeoVim these days.

Hell, Vim even has floating panels now that NeoVim does not if you are into that sort of thing. NeoVim simply fails to provide a significant alternative value proposition.

## What About Graphical Vim?

Sure it's nice, but the main reason to learn a terminal editor is to take advantage of the [powerful](#magic) possibilities of full shell integration, which is simply not possible from the graphical version --- especially when running from Windows.

:::

## Installation

The `vi` command is already on your Unix/Linux machine. That's the beauty of it. However, you might want to get the full version of `vim` if this is your workstation or you want to use `vimtutor` for learning.

```sh
sudo apt update
sudo apt install vim
```

## Configuration

Configuring Vi/m is critically important to be effective. It is usually good to start with [someone else's `.vimrc`](vimrc) and alter that to your needs.

:::co-tip
Keep your `.vimrc` configuration light-weight and portable. When done [properly](vimrc) you won't even have to install any additional plugins, they'll be installed automatically.
:::

## Plugins

Vim plugins are a great source of power when used judiciously. The leading plugin manager is currently [Plug](https://github.com/junegunn/vim-plug), which can be [automatically downloaded](vimrc) and called on to install plugins when it is first launched in a way that does not interfere with other [dotfiles](https://duck.com/lite?q=dotfiles) that you might have saved in a [Git repo](https://duck.com/lite?q=Git repo).

## Learning

The best way to learn Vim is to use Vim. (Now there's a surprise.) This is why [`vimtutor`](https://duck.com/lite?q=`vimtutor`) is currently the beset learning tool. It isn't complete and has flaws but is better than anything else because it actually *is* Vim. After that maybe try some of the others in the [resources](#resources).

:::co-warning
Make absolutely sure that you properly learn to use [shell integration with the magic wands](#magic) before you conclude your beginner Vi training. You simply do *not* know Vi without them and *every single tutorial on the planet leaves them out*. You seriously *cannot* say you know Vi without understanding the single most powerful feature of the world's most ubiquitous editor. There is little need for additional Vim plugins when shell integration is properly understood.
:::

## Vi Survival {#survive}

People like to joke about how hard it is to exit Vi. It's true that for years people would be suddenly forced to use Vi without expecting it (editing a `crontab` was a common one). (Now that Nano is the default on all major Linux [distros](/distros/) Vi users are the ones stuck not being able to exit.) Here are the absolute basics to help you survive your first encounter with Vi/m with a smile.

------------------ ---------------------------------------------
 Action            Description
------------------ ---------------------------------------------
 `h`               Move left.

 `j`               Move down.

 `k`               Move up.

 `l`               Move right.

 `i`               Change into `INSERT` mode for editing.

 (Arrow Keys)      Move in insert mode. (Vim noobs only)

 `ESC`             Escape back into COMMAND mode. (Not shown).

 `Control`+`[`     Same as `ESC` but easier to reach.

 `ZZ` or `:wq`     Save and quit from COMMAND mode.

 `ZQ` or `:q!`     Quit without saving from COMMAND mode.

 `:w`              Save without exiting.

 `:help`           Bring up the internal Vim help system.
------------------ ---------------------------------------------

:::co-note
You cannot move around until you have added a line or two to the file.
:::

## Shell Integration with Magic Wands FTW!{#magic}

*Magic wands* are a mnemonic device for using the [exclamation point](/typing/#symbols) to send the current line, section, or page to any command that can be run from the shell and replace those lines with the output of that command. In fact, once you master the magic wands you can use the same keystrokes to do find and replace and other tasks that use `ex` mode just by backspacing out the exclamation point.

:::co-comment
Yeah, you don't need all those Vimscript macros. No really, you don't. Just use Bash instead.
:::

### Line Wand

The line wand is by far the most frequent wand you will use. It is the fastest and most common way to extend `vi`. Just spam bang twice to send the current line to the shell command or just replace the current line with the output of a utility command like `date`, `cal` or *any* text that comes out of *any* command.

#### Send Line to Bash: `!!bash`

```bash
for i in {1..20}; do echo Item $i; done
```

#### Send Line to Calculator: `!!bc`

:::co-warning
This requires the `bc` program to be installed, which is by default on most Linux systems. If not [install it](/clinstall/).
:::

```bc
232432 * 2342342 / 234
```

#### Send Line to Python3: `!!python3`

```python
[print(f"Item {x}") for x in range (1,21)]
```

### Section Wand

Sending a section is a great way to run part of a larger script (say for configuration) without having to cut it out and put it into another file. If you use this easy way, don't forget to undo it later to replace it with the original code (instead of the output of the lines of code).

#### Send Section to Bash: `!}bash`

```bash
for i in {1..20}; do
  echo Item $i
done
```

### Line Number Wand: `!:<lineafter>`

Line number wand is just a larger version of the [section wand] but allows blank lines to be included. It takes more keystrokes, however. Start with a bang and follow it up with a colon and the line *after* the last line that you want to include.

For the following example imagine the first line is line 10 of the file and you want to send it to `pandoc` for rending as HTML. You would position your cursor on the first `#` (on line 10) and type `!:22<enter>` (because there are 11 lines to send). Then type `pandoc<enter>` after the `!` as the command to receive the lines and replace them.

```markdown
# This is a title with *emphasis*

* one
* two
* three

How about a paragraph

## Thursday, March 12, 2020, 7:46:39PM

something
```

```{.out}
<h1 id="this-is-a-title-with-emphasis">This is a title with <em>emphasis</em></h1>
<ul>
<li>one</li>
<li>two</li>
<li>three</li>
</ul>
<p>How about a paragraph</p>
<h2 id="thursday-march-12-2020-74639pm">Thursday, March 12, 2020, 7:46:39PM</h2>
something
```

## Resources

* `vimtutor`
* `:help` (from within Vim)
* <https://openvim.com>
* <https://vimgenius.com>
* <http://yannesposito.com/Scratch/en/blog/Learn-Vim-Progressively/>

:::co-warning
Stay away from Vim Adventures. As fun as it is, it does not make a direct association to editing actual files that these other resources do.
:::
