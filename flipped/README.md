---
Title: Flipped Course
Subtitle: Learn and Practice at Home, Discuss and Collaborate Together
tpl-hdsearch: true
---

A *flipped* course (or classroom) is when learners review content and practice things *before* meeting together to discuss it and share. Time together is dedicated to discussions, show-n-tell, interactive learning activities, and doing stuff together that might otherwise have been done at home but with the guided expertise of an experienced mentor and other members of the learning community.

:::co-warn
Flipped courses fall on their faces if people don't do the work and research ahead of time. When this is the case the model quickly reverts to traditional lecture and homework assignments instead. It takes dedicated effort and a trusted learning community to pull off the flipped learning model, but when it works the result is [autodidacts](/autodidact/) who are *far more prepared* for other learning later.
:::
