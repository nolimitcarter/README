---
Title: GitHub Flavored Markdown
Subtitle: Run Screaming from This Monstrosity
tpl-hdsearch: true
---

*GitHub Flavored Markdown* might be many people's first experience with Markdown, but it is seriously demented, broken, and incompatible with other, better standards --- tables are *particularly* bad. Do *not* use GFM! Use CommonMark (the industry standard), which does not include tables at all, or use [Pandoc Markdown](/pandocmark/) instead.

:::co-mad
People have actually wasted their time and energy creating tools just to create GFM tables, that's how stupid it is. It defeats the whole premise and promise of Markdown in the first place.
:::
