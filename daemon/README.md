---
Title: Daemons
Subtitle: Your Server Servants
tpl-hdsearch: true
---

A daemon is a lesser demon that does the bidding of its master, which is exactly what a running program that listens for requests and commands is in the UNIX/Linux world. You will notice some programs have a `d` after them, such as `sshd`, which is not the same as `ssh`. The first is the thing that you connect to. The later the client program that does the connecting.

:::co-fun
There have actually been Christian groups organize protests against BSD because of the reference to daemons and 'kill'ing things.
:::
