---
Title: Linux Beginner Boost
tpl-notitle: true
tpl-notitlehead: true
---

<div class=boxpoint>
[**Welcome to <span class=spy>RWX.GG</span>,**]{.biggest}  
<span class=bigger>the [knowledge app](/pka/) for  
*Linux Beginner Boost* and more.

<span class=big>Beginner boost is a [flipped](/flipped/) and [intense](/schedule/) [open course](/opencourse/) and [credential](/cred/). Chances are you haven't experienced anything like it in traditional education. You'll need more than just this app to get you through it. But don't worry. Join our community.  
We got you.

[**So what'll you learn?**]{.bigger}  
First you'll learn how to set yourself up to be an effective, tech-focused, [autodidact](/autodidact/). After learning how to learn you'll master the Linux command line as you write *real* code for projects in [nine different languages](/languages/), from low-level to high. Your portfolio will prove your skills and knowledge to everyone and anyone --- including you. You'll not only know how to code remotely on any computer in the world, but how computers and languages work. Later you'll be able to learn on your own --- especially coding languages and Linux distros --- because you just did. You'll send your [impostor syndrome](/impostor/) packing and armor yourself against [Dunning-Krueger](/dk/) making you that much better as a candidate for any occupation to choose to pursue later. What comes next is up to you. 


[**What does it cost?**]{.bigger}  
Nothing. This app and course will always be [free](/copyright/). You pay the cost of any [books](https://rwx.gg/books/) you choose to buy. This project is possible through generous donations and volunteer contributions from a great community of veterans and beginners. [RWX](/rwx/) is an open *knowledge* source project. One that seeks to crumble crusty cronyism and dispatch the deadly dogma that has suffocated *real* learning for far too long.

</div>

<style>

  main {
    max-width: unset;
  }
  
  main.content {
    text-align: center;
    margin: 0;
  }

  .boxpoint {
    font-family: var(--sans-font);
    font-size: 1.4em;
    text-align: center;
    line-height: 1.5em;
    max-width: 600px;
    display: inline-block;
    margin-top: 2em;
  }

  .biggest { font-size: 1.3em; }
  .bigger { font-size: 1.15em; }
  .big { font-size: 1.02em; }
  .small { font-size: .9em; }
  .smaller { font-size: .8em; }
  .smallest { font-size: .7em;line-height:0.8; }

  footer {
    display: none;
  }

  a {
    color: none !important;
    text-decoration: none;
    background: none !important;
  }

  a:hover {
    text-decoration: dotted;
    background: none !important;
  }

</style>

