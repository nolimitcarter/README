---
Title: Progressive Knowledge Apps
Subtitle: Better Than Books and Wikis
tpl-hdsearch: true
---

A *progressive knowledge app* is a [minimum viable knowledge base](/mvkb/) written entirely in [Markdown](/md/) and rendered as a [progressive web app](/pwa/) but can equally be rendering in any number of consumable media formats including ePub, Kindle, PDF, and others. Modern web browsers support service workers allowing PKAs to contain their own localized search engines without need for an Internet connection at all once they've been downloaded.


