---
Title: Get a Job
Subtitle: And Never Stop Searching
tpl-hdsearch: true
---

Getting a job isn't hard if you are prepared, confident, and have contact with those who need what you have. You simply need to gain their trust by *proving* what you can do for them. Every single position and potential employer will have a different idea about what that proof looks like. 

Here are a few tips to help.

* Don't be passive
* Do the research
* Be on the hunt
* Get on Linkedin
* Always be closing
* Find a recruiter

## Resources

* [Elements of Programming Interviews](<https://elementsofprogramminginterviews.com/>){.see}
