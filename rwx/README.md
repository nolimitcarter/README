---
Title: RWX, Mantra of the Autodidact
Subtitle: Read/Research. Write. Execute/Exercise/Experience.
---

RWX is an ancient fix to a [broken](/corrupt/) modern education system. Learning isn't that complicated. We humans have been doing it for thousands of years. But greed and power have corrupted and destroyed many of our systems of learning and sought to *control* our learning in ways that benefit everyone *but* the person learning.

The transfer of knowledge from one person to another has a rather simple formula. Those who master it become powerful [autodidacts](/autodidact/).

* [**Executing, Exercising, and Experiencing**](/x/)  
We gain experience by doing things, trying things, making mistakes, and correcting ourselves, sometimes with the help of others who already have that experience.

* [**Writing**](/w/)  
We capture and share our learning experience through writing, image, and multimedia.

* [**Reading and Research**](/r/)  
We read and research what others have written about their learning and experiences and recreate them for ourselves in our own way.

This cycle repeats both individually and collectively. Usually we enter the cycle when we learn of others experience through reading. Hence, RWX.
