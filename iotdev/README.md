---
Title: IoT Embedded Device Developer
Subtitle: Protecting Us from Our Toasters
tpl-hdsearch: true
---

It's hard to pin down this new occupation in the industry. It has elements of an [Electrical Engineering](https://www.bls.gov/ooh/architecture-and-engineering/electrical-and-electronics-engineers.htm) as well as [Software Development](/dev/). It's fair to say that the education and salary are comparable to a [Computer Hardware Engineer](https://www.bls.gov/ooh/architecture-and-engineering/computer-hardware-engineers.htm) (Bachelors, $117,220).
