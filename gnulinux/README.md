---
Title: Don't Say GNU/Linux
Subtitle: Honestly It Disrespects *Everyone's* Contributions
tpl-hdsearch: true
---

Initially it makes sense to call *Linux* what Stallman demands. But he's wrong. So many people have contributed to everything contained in modern Linux that it has become disrespectful to single out *just* the GNU contributions. 

:::co-rage
The unethical handling of the GPLv3 license release might just kill GNU software forever due to its overreaching requirement affecting hardware, something that was never a part of the original licensing and that threatens to force many from picking other software simply because the size of the devices running Linux is becoming too small for a license that *forces* hardware manufacturers to provide some way for the GNU software to be updated to anything that is running it. This short-sightedness by the FSF who created it is also the reason they cannot see all the other contributions from thousands of others that are *not* under GNU. So no, Stallman, your argument might have held water for a few years but has now bee obliterate like your reputation and modern relevance. Run along and play with your Emacs code some more.
:::


