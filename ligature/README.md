---
Title: Ligature
tpl-hdsearch: true
---

A *ligature* is when two characters are combined into a single new character. So for example with `>=` is combined into ``≥.
