---
Title: Typing / Keyboards
Subtitle: Grok the Keyboard Like a Coder
tpl-hdsearch: true
---

You can't do anything these days without being able to use a keyboard. The selection of a keyboard, however, is a *very* personal decision.

## Symbols

Technical occupations require not only knowing about the lesser-known symbols on the keyboard but being able to type them relatively quickly. Most typing games unfortunately do not take this into account. Here they with some of their slang nicknames so you can know what people are talking about.

  Symbol      Name
--------- --------------------------------------------
   `/`        Slash / Wack
   `\`        Back Slash / Wack
   `.`        Dot
   `!`        Exclamation Point / Bang
   `?`        Question Mark
   `:`        Colon
   `;`        Semicolon
   `|`        Bar / Pipe
   `^`        Carrot
   `*`        Star / Asterix / Splat
   `-`        Dash / Hyphen
   `_`        Underscore
   `~`        Tilde / Squiggle
   `=`        Equals Sign
   `+`        Plus
   `#`        Hashtag / Pound / Octothorpe
   `%`        Percent
   `$`        Dollar Sign
   `''`       Single Quotes
   `""`       Double Quotes
   ` `` `       Backticks
   `@`        At Sign
   `&`        Ampersand
   `[]`       Square Brackets
   `{}`       Curly Brackets
   `<>`       Angle Brackets
   `()`       Parens

:::co-fyi
You might be amazed how many people actually do not even know where the semicolon is on their keyboard, let alone the backtick.
:::

Also note the use of the following for the meta keys:

* `Space`
* `Control`
* `Alt` or `Option`
* `Super` or `Command` or `Windows`
* `Shift`
* `Return` or `Enter`
* `Escape` or `Control`+`[`
* `Tab`
* `Fn`
* `Home`
* `End`
* `PageUp`
* `PageDown`
* `Delete`
* `Backspace`
* `Insert`
* `F1` to `F19`
* `1` to `0`

There are a number of special keys for sound and such. Usually these are activated by default and to get function keys to work these days you need to hold down the `Fn` key. This can get pretty tricky when combining with `Option` and `Control` at the same time, for example, when trying to pull up a pseudo-tty (`Control`+`Option`+`F1` and `Control`+`Option`+`F7` to get your graphical desktop back)

:::co-warning
Keep in mine that what appear as the same keys on a numeric keypad are *not* the same keys when subscribing to key-press events.
:::

:::co-tip
One of the most frequent tough situation beginners get into is by accidentally hitting the `Insert` key on their keyboard. This switches them into overwrite mode which just writes over their other text instead of behaving as usual. Just press the `Insert` key again to see the cursor change back.
:::

## Discussion Questions

* What is the minimum typing speed to work in technology?
* Do I *have* to learn touch typing?
* Should I customize my keyboard?
* What is the best keyboard?
* What about Dvorak?

## Resources

* <https://typing.com>
* <https://zty.pe>
* <https://nitrotype.com>
* <https://www.ostechnix.com/wpm-measure-typing-speed-terminal/>
