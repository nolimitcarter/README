---
Title: Modern C
Subtitle: Good and Free but Not Beginner Friendly
tpl-hdsearch: true
---

*Modern C* is a absolutely free book about the most modern version of the C language. It was last updated on Oct. 10, 2019 and [written by a French author Jens Gustedt](https://modernc.gforge.inria.fr/). Although it is not nearly as beginner-friendly nor does it contain as much content as [Head First C](/books/hfc/) it is a useful resource and has [been included here as a PDF](modern-c.pdf) for reference.
