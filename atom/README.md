---
Title: Atom Text Editor
tpl-hdsearch: true
---

The [Atom](https://atom.io) editor was [GitHub](/github/)'s answer to the revolutionary [Sublime](/sublime/) light-weight text editor. The [Electron](/electron/) framework was built specifically *for* Atom. The goal was to use *only* web technology instead of C++ and Python which is what Sublime is built on. While many people prefer Atom today over the far more popular and powerful [VSCode](/vscode/) it lacks some rather significant necessities according to most developers. Atom is not recommended for this course mostly because we are focusing on editing code with [Vim](/vi/) above all and secondarily with web tools and VSCode.
