---
Title: Server
Subtitle: Serves Client Requests
tpl-hdsearch: true
---

A *server* is something a [client](https://duck.com/lite?q=client) connects to over a [network](/networking/). For example, [Chrome](https://duck.com/lite?q=Chrome) is a *client* that connects to [web servers](https://duck.com/lite?q=web servers). [SpigotMC](https://duck.com/lite?q=SpigotMC) or [PaperMC](https://duck.com/lite?q=PaperMC) are servers for [Minecraft clients](https://duck.com/lite?q=Minecraft clients).
