---
Title: Command Line Package Installation
Subtitle: We Don't Need No Stinking Graphics
tpl-hdsearch: true
---

It is far easier and less error prone to install software packages from the command line, but the method of doing so differs dramatically between different [Linux distributions](/distros/). Here's a summary for the most common distributions you might be using:

## Debian

`sudo apt install <package>`

