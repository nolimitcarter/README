---
Title: Contributing
Subtitle: Help Out Our Knowledge Development Team
---

Contributing to [RWX.GG]{.spy} can take many forms. Perhaps you want to write content and submit a merge request, maybe you want to help moderate the chat, or perhaps you just want to donate money or other resources to the cause.

## Submitting Content

Anyone is welcome to submit content that is relevant to overall course and site [goals](/goals/). Simply submit a pull request or [open a ticket](https://gitlab.com/rwx.gg/README/-/issues) with suggestions or bug fixes that are needed.

## Do Your Own Stream

Another way to contribute is to do your own streams. Streamers who regularly stream their own interpretation of this content and their working through the course can simple open an issue requesting a link on our [streamers](/streamers/) page.

## Donations

Currently the only way to donate is to subscribe to [rwxrob.live](https://rwxrob.live) or send money [directly](https://paypal.me/rwxrob). This may change in the future as our project grows.

## List of Content Contributors

* MousePotato
* zerostheory
* mtheory11dim
* elremingu
* unres1gned
* elementhttp
* OGLinuk
* Jovan Vladislav
* smartrefigerator
* (everyone else, let us know)
