---
Title: IT Services
tpl-hdsearch: true
---

In tech, the word *services* can refer to many things but usually means IT services, system services, or cloud services.

 Reference             Definition
---------------------- --------------------------------------------------------
  IT Services          Services provided to a company from the IT department.

  System Services      Running programs that listen for connections and do
                       other ongoing work for the system. (Also Daemons)

  Cloud Services       [IT services](/cloud/) provided securely over the
                       Internet rather than locally.
