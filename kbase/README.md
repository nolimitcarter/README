---
Title: Knowledge Base
Subtitle: Better Than Books 
tpl-hdsearch: true
---

A *knowledge base* is simply a collection of structured and unstructured information in a way that can be consumed, shared, and queried. This [knowledge app](/pka/) is built on a [rotating release](/rotating/) knowledge base.
