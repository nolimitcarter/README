---
Title: Fail Faster
Subtitle: More Fail, Less Futz
tpl-hdsearch: true
---

> People are used to a culture that doesn't tolerate failure, but failure is an essential ingredient for radical innovation. (Sunnie Giles, Forbes)

> Now let me tell you what failure is not. Failure is not a part of learning, as in trial and error. When a baby is learning to walk, takes its first step, falls down and bursts into tears, that’s not failure. If the baby never gets back up and succeeds in learning to walk, that’s failure. But making mistakes as we learn new things is not. It’s simply the way we learn. (Steve Tobak, Entrepreneur)

Speed of execution over quality, those are the priorities sometimes cited of those aspiring to *fail faster*, creating things that are *good enough* rather than perfect. 

Unfortunately this is a *very* delicate balance. It is easy to misinterpret this in a way that practically makes failure the goal, instead of a way to success. 

Mistakes are not failure. Mistakes without correcting them is.

The priorities of a business manager might be a deadline motivating them to invoke *fail faster* when it puts the entire system and company in jeopardy. Failing faster does *not* mean removing quality assurance from the process, just lessening the priority on it *knowing* you can always create a new version almost immediately that addresses any moderate failures --- just not [catastrophic]( https://edition.cnn.com/2019/07/22/tech/equifax-hack-ftc/index.html) ones.

## Resources

[Why 'Fail Fast, Fail Often' Is All Hype](https://www.entrepreneur.com/article/288147){.see}
[How to Fail Faster](https://www.forbes.com/sites/sunniegiles/2018/04/30/how-to-fail-faster-and-why-you-should/){.see}


