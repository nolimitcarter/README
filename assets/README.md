---
Title: Assets
---

An *asset* is anything of value but in tech it generally means files that are used elsewhere --- usually images, audio, video, 3D models, documents, fonts and such. Assets are distinct from your code. They are resources your code loads and controls in your application.

:::co-tip
It is common to have an `/assets/` directory to hold all your web site stuff away from all the informational content --- especially when creating a [knowledge base](/kbase/) such as this one with common files like the following:

   File                                   Description
------------------------------------  ---------------------------------------
 [`template.html`](template.html)       Single Pandoc template file
 [`styles.css`](styles.css)             All CSS styles
 [`main.js`](main.js)                   All JavaScript
 `img`                                  Common images
 `fonts`                                Fonts 

:::
