---
Title: Style Guide
Subtitle: Follow These When Contributing
---

Here's a summary of styles used on `rwx.gg` so that contributors can keep things *extremely* consistent. The value proposition of this knowledge app (above, say, a wiki) is this consistency. Feel free to [open an issue](https://gitlab.com/rwx.gg/README/-/issues) if you feel that anything violates this consistency. Thanks.

## Writing Style, Grammar, and Usage

The `rwx.gg` project strictly follows [The Gregg Reference Manual](/books/gregg) for all written content.

## General Rules

* Make template at `/assets/template.html`

* Keep nodes composable, don't include too much into one.

* Never use `H1` (`#`) headings.

* Only `README.md` files are rendered. Other markdown ignored.

* `template.html` within any node will *override* main template.

* Subnodes of [knowledge nodes](/knode/) are fine.

* Strictly limit node and subnode directory names to under 12 characters.

* Keep node directory names easy to type:
    * Lowercase
    * Letters and numbers
    * No hyphens
    * No emojis

* Use a default empty link populate line in your `.vimrc` to fill in empty links that pull up searches on Duck.com rather than not link at all. Later you can return and provide local content as time allows. This saves those learning on the site from having to type in all the links to search the Web for terms that are likely to need explanation and allows mobile users to simple tap as they go.

```vimscript
autocmd vimleavepre *.md !perl -p -i -e 's,\[([^\]]+?)\]\(\),[\1](https://duck.com/lite?q=\1),g' %
```

## Callout Types

Here are the different callout types that can be used by adding a `co-` to the beginning. Make sure you know what a Pandoc Markdown "Div" is. (There are a lot of examples already in the content.)

 Identifier    Description
------------   -------------------------------------------
 `faq`           Surround level two headers of FAQs
 `fyi`           Informational but not related
 `btw`           Informational and slightly related
 `fun`           Informational, related, and fun
 `yea`           Something to celebrate.
 `tip`           High value information, related ProTips
 `chk`           Something specific to check for
 `care`          Take care, be careful
 `warn`          A critical warning
 `stop`          Stop and pay attention, achtung
 `rage`          A reason to get angry 
 `dumb`          So dumb you'd smack your forehead

## Special `faq` Divs

In order for frequently asked question text to be included in the main meta data summations that are used for localized searches all FAQ questions must begin with a level-two heading (`##`) and should also be grouped and encapsulated in a div group as follows:

```markdown
:::co-faq

## How should I include FAQs throughout the content?

This is how

## Do I need them to always been second level headings?

Yes. Nothing forces it, but it is a good convention.

:::

```


## Special Link Types

The following link types can be added to a link as `{.<type>}` immediately after the link markdown causing the link to have extra formatting and `before` content including an emoji or special character.

 Identifier    Description
------------   -------------------------------------------
 `see`           Notable internal or external link
 `watch`         Direct link to a video to play 
 `download`      A link to a resource that will download


