---
Title: Free Software Foundation
Subtitle: Descending Into Madness
tpl-hdsearch: true
---

The Free Software Foundation, headquarters in Boston, was started by Richard Stallman in 1985 and maintains the "copyleft" licenses of the [GNU project](https://duck.com/lite?q=GNU project). The FSF was sharply criticized by Linus Torvaldz (creator of the Linux kernel) who "doesn't want to have anything to do with the FSF ever again" after their handling of the [GPLv3 license](https://duck.com/lite?q=GPLv3 license) roll out.

## Resources

* [Linus Explains Why Against GPLv3 and FSF](https://youtu.be/PaKIZ7gJlRU){.see}

