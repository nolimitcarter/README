---
Title: Basic Markdown
Subtitle: Simplicity and Compatibility
---

Here is quick overview that you can complete in 20 minutes covering the most basic, compatible [Markdown](/md/) everyone should learn. The priority is on simplicity and maximum compatibility. It allows your content to be used *everywhere*. There is no more ubiquitous form of knowledge storage.

## Paragraphs

Paragraphs are just long, single lines that are followed by a blank line. That way they wrap properly no matter what editor you are using or its width. Put one blank line after each paragraph to separate it from the rest. 

Paragraphs can contain formatting.

![Example of Good Wrapping](good-wrap.png)

Although every Markdown tool allows multiple lines to be grouped into a single paragraph so long as a they are followed by a blank line this practice is rather bad when editing those same files with editor or terminal or [tmux](/tmux/) pane widths that are less than the line length. They are completely unreadable. Stay safe, just use single long lines. All modern (and even most ancient editors) support auto-wrapping the line in the terminal rather than effectively hard wrapping it in the display.

![Example of Bad Wrapping](bad-wrap.png)

## Formatting

One star for *italics*:

```markdown
one star for *italics* 
```

Two stars for **bold**:

```markdown
two stars for **bold**
```

Three stars for ***bold italics***:

```markdown
three stars for ***bold italics***
```

Backticks for `code` also known as `monospaced`:

```markdown
backticks for `monospaced`
```

:::co-warning
Do not use underscore (\_) ever. Yes the original Markdown supported it, but it was always a bad idea. Eventually it will bite you. It is not widely supported and causes problems when used for actual names.

Also, avoid combine or overlap formatting as much as possible and avoid formatting a portion of a single word, which is not handled consistently --- especially by syntax-highlighting tools and editors.
:::

## Escaping

Any time you don't want, say, a star (`*`) to be recognized by the Markdown parser just put a backslash in front of it. This goes for all the other special symbols used in Markdown. In fact, you can put a backslash (` \ `) in front of any character whatsoever in Pandoc Markdown to cause it to be used as is.

:::co-warning
One particularly common character to need escaping is the dollar sign (`$`). This is because Pandoc supports the inclusion of LaTeX for math notation and uses the dollar sign to mark it. Jut remember to always put a backslash in front of it and you are fine.
:::

## Headings

Headings (often incorrectly called headers) begin with 1-6 hashtags (`#`) followed by a space and then the title text followed by a single blank line. 

```markdown
# Level One

Paragraphs and such here.

## Level Two

Paragraphs and such here.
```

Formatting is allowed in headings but can be problematic with some renderers. Avoid if you can.

:::co-tip
Generally you should never have more than one first level heading (`# Heading One`) because search engines prioritize it. When using Pandoc you will not even need a level one heading because the `Title` is better placed in the meta-data property instead and rendered with the Pandoc Template.
:::

## Links / Hyperlinks 

Hyperlinks (stuff you click on) come in three basic forms: words, URLs, and images.

### Hyperlinked Words

The most common link in Markdown is just [words you can click on](https://rwx.gg). The web address must be either pointing to a remote site or so something on the same site that document is on.

```markdown
Here is a [link to rwx.gg](https://rwx.gg).
```

:::co-warning
Never use "reference links" that allow you to put the actual link elsewhere in the document (usually at the bottom). While this made sense when Markdown was invented and the emphasis was on how pretty and readable the Markdown source text itself was this emphasis has changed today. Separating your link several hundred lines away from the text that is linked is *really* bad practice because it decouples the link making it much harder to copy and paste to another document --- or worse --- you will edit your document and completely forget to remove them leaving them orphaned.
:::

### Autolinked URLs

Sometimes you want the web address to actually appear like <https://rwx.gg/md>.

```markdown
Here is URL to <https://rwx.gg/md> that will appear in full.
```

:::co-warning
Never trust that your external links will be detected and autolinked automatically just because they start with `http`. There are still Markdown engines out there that do not autodetect them (for good reason). This is one of the dumbest things GitHub added to their flavor of Markdown.
:::

This also works with other link types besides `http`. (Yes, there are several other URL schemas.)

```markdown
Mail me at <mailto:rwx@robs.io>.  
Phone me at <tel:555-555-5555>
```

## Images

Images are just links with an exclamation point in front. Make sure to put a blank line before and after any image for maximum compatibility. Inline images are not widely supported and mess up other formatting in almost all cases.

```

![gnome](/assets/img/mr-rob-gnome.jpg)

```

Remote image (but usually bad practice):

![](https://rwx.gg/assets/img/mr-rob-gnome.jpg)

Images can also be used as links.

```
[![RWXROB](https://rwx.gg/assets/img/mr-rob-gnome.jpg)](https://robs.io)

```

[![gnome](https://rwx.gg/assets/img/mr-rob-gnome.jpg)](https://robs.io)

## Lists

Simple lists are supported by pretty much everything. Put the list items one to a line. Make sure to put a blank line after the list. 

Use stars followed by spaces (`* `) for bulleted (unordered) lists:

```markdown
* some
* thing
* here

```

* some
* thing
* here

Use the number one followed by a period and a space (`1. `) for numbered (ordered) lists:

```markdown
1. HTML 
1. CSS
1. JavaScript
1. Go
1. Bash
```

1. HTML
1. CSS
1. JavaScript
1. Go
1. Bash

Always use `1.` so that if you change the order you do not have to renumber the source itself. It will automatically change the number order when rendered.

## Separators

Also called "horizontal rule." These just break up the page usually with a horizontal line.

```markdown
----
```

----

Use four dashes for consistency even though there are dozens of ways to indicate separation (some of which allow stars to be used as well). This consistency allows you to easily find your separators and keeps them from being confused with YAML markers (which use three dashes) and inline formatting (which uses stars `*`).

## Hard Returns

Hard returns are a way of starting a new line within a given paragraph. Type two spaces (`␣␣`) followed by the line return.

```markdown
Roses are red␣␣
Violets are blue
```

Rose are red  
Violets are blue

:::co-tips
The [Pandoc VIM plugin](/vi/#plugins) is particular useful at showing these with [ligatures](/ligature/).
:::

![](pandoc-hb.png)

## Blocks

Blocks separate text or code from the document usually as a box. There are two main block types to remember: *plain* (preformatted, as-is) blocks and *code fences*. Both use three [backticks](/typing/#symbols) to "fence off" the text or code.

### Plain

When you just want the text to appear exactly as it is just use the triple-backtick fence posts.

~~~markdown
```
    Roses are red
    Violets are violet
```
~~~

```
    Roses are red
    Violets are violet
```

### Code Fences

Code blocks are perhaps the single biggest reason to use markdown for all your tech writing and note taking. Usually the code will automatically be syntax highlighted for you. This provides very high-quality publications very easily or just amazing personal logs and notes.

When you want to add syntax highlighting or otherwise indicate how the text should be handle provide an information tag immediately following the first triple-backtick fence, so for JavaScript it would be:

~~~markdown
```js
console.log('hello world')
```
~~~

```js
console.log('hello world')
```

:::co-fyi
Note that I do not have color syntax highlighting active here because I find it distracts and doesn't print well when printed copies of lessons are needed, which they are in many educational settings.)
:::

Although there are other ways to write blocks, using triple-backtick fences is the most consistent way to do them all. This allows quickly finding your blocks when editing as well as filtering them out easily with scripting or simple parsing. It is also the most widely supported. Discord, for example, only supports this format of code fence.

Here is a short list of supported language tags:

 Tag         Language
---------- ---------------------
 md          Markdown 
 json        JSON
 js          JavaScript 
 html        HTML 
 css         CSS 
 sh          Shell or Bash

#### Exception for Markdown

In the single exceptional case where you need your block to contain markdown code you should use three or four tildes (`~~~markdown` or `~~~~markdown`). Again, this consistency allows you to filter out blocks from simple scripts that examine each line which can be useful for coding keyword searches and such.

~~~~
~~~markdown
Here is *some* markdown.

```js
console.log('hello example')
```
~~~
~~~~

~~~markdown
Here is *some* markdown.

```js
console.log('hello example')
```
~~~

:::co-mad
There are literally an infinite number of possible ways to indicate a block supported by the original and most derived Markdown parsers. Just stick with these two options. Consistency is far more important than artistic expression. Blocks are particularly important to keep consistent because you will frequently want to simply strip them out for keyword searches and such. Following these suggestions makes this trivial even from simple shell scripts.
:::

:::co-warning
Make sure there is no space after the backticks and before the block identifier (`js` in the example).
:::

:::co-fyi
Technically paragraphs, lists, and even separators are also considered blocks when parsed.
:::

## Blockquotes

Blockquotes are for quotations and *only* quotations. Avoid the temptation to use them for anything else because if you do you can semantically identify all the *actual* quotes in your content.

Begin each line of the block with a greater-than sign (right [angle-bracket](/typing/#symbols)).

Usually you will just have a single paragraph:

```markdown
> "One of the painful things about our time is that those who feel certainty are stupid, and those with any imagination and understanding are filled with doubt and indecision." (Bertrand Russell)
```

> "One of the painful things about our time is that those who feel certainty are stupid, and those with any imagination and understanding are filled with doubt and indecision." (Bertrand Russell)

:::co-tip
Use of quotation marks surrounding the text of the quote itself is completely up to you but is recommended so that multiple quotes can be combined next to one another without reader confusion.
:::

In the rare case that your quotation expands beyond a single line make sure to join separate paragraphs with a blank line that is also included:

```markdown
> This is the first part of the quote.
>
> Here is the second part.
```

> This is the first part of the quote.
>
> Here is the second part.

## Tables

Just don't use them. If you do need them, use [Pandoc Markdown](/pandocmark/) instead (not [GitHub Flavored Markdown](/gfm/)). For *basic* Markdown it is more important to maintain compatibility and no one agrees on how tables should be done. It is one of the most hotly debated topics in the Markdown community.

:::co-mad
Absolutely *do not use* GitHub's brain-dead table format, which is so stupid people have actually written tools to generate them defeating the entire purpose of Markdown in the first place. GitHub tables are a GitHub addition and were *never* supported by Markdown and are not supported by CommonMark. So if you ever want to move to another Git hosting site (like GitLab, and you should) you will quickly discover your tables do not render. If and when you are ready to be okay graduating to a more powerful Markdown syntax always use Pandoc Markdown instead, which has much broader and deeper support and includes several much more powerful and simple table formats that GitHub's.
:::
