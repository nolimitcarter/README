---
Title: Grokking, Installing, and Running Linux
Subtitle: Week Two
---

* [Day 6]
* [Day 7]
* [Day 8]
* [Day 9]
* [Day 10]

## Day 6

[📺]{.noinvert} [Linux Beginner Boost - Day 6 (May 11, 2020)](https://youtu.be/v9z-FqNPX90)

### What is Linux?

* Grok Why and Where Linux Exists 
* Meet Richard Stallman, Creator of GNU
* Meet Linus Torvaldz, Creator of Linux
* Grok Why GNU/Linux is Disrespectful
* Grok Why Mac is Not Linux
* Grok Why Windows is *Becoming* Linux

### Getting Linux 

* Review Linux Terminal from Week One
* Grok Cloud vs Virtual Machine vs Hardware

### Licensing and Legal Considerations

* Don't Taint Yourself
* Know What You're Signing
* Grok Free Software Licenses
* Grok Open Source Licenses
* Pick Apache V2 License for Permissive
* Pick GPLv2 License for Copyleft
* Know Difference Between GPLv2 and GPLv3
* Avoid GPLv3 in General
* Grok GPLv3 Implication for Embedded
* Grok Creative Commons Licenses
* Grok Open Core Business Model

### Certification

* Know Why and When to Certify (It's About Trust)
* Grok Proof is in the Projects, Not Paper
* Discuss LPI Linux Certifications
* Leverage LPI Outlines for Learning
* Discuss Alternatives to Certification
* Avoid CompTIA for Breech of Ethics
* Watch Out for Cert Scammers

### Linux Distributions

* Know the Main [Distros](/distros/)
* Grok Why Linux from Scratch is Not a Distro
* Pick the Right Distros
* Debate Distros

## Day 7

[📺]{.noinvert} [Linux Beginner Boost - Day 7 (May 12, 2020)](https://youtu.be/2TYTrsK_ry0)

### Know the Linux Hosting Providers

* Meet Amazon Web Services
* Meet Google Compute Engine
* Meet Microsoft Azure
* Meet Digital Ocean
* Meet RackSpace
* Meet Linode

### What Can You Do?

* Host an Agar.io Clone
* Host a Doom server
* Host Your Own Websockets Game
* Host Your Own Database with GraphQL API
* Grok Why Hosting Web Site is Bad Idea
* Grok Appeal of Hosting APIs

### Create a Digital Ocean Droplet

* Set Up SSH Keys
* Create a Digital Ocean Account
* Create the Droplet
* Login Remotely

### Set Up Paper Minecraft Server

* Download Server with Curl
* Grok Minecraft Licensing
* Start Up in Screen or TMUX
* Give Op
* Connect and Try It Out

## Day 8

[📺]{.noinvert} [Linux Beginner Boost - Day 8 (May 13, 2020)](https://youtu.be/yraQ7kILVkE)

### Grok [Virtualization](/virt/)

* A Machine Within a Machine
* IBM Invented It
* VMWare Popularized It and Won the Enterprise
* VirtualBox Won the Open Market

### Grok [Containerization](/contain/)

* Houses and Apartments Analogy
* Docker to Create Container
* Kubernetes to Manager the Containers

### Run a Linux Docker Container

* Download and Install Docker
* Authorize Your User
* Create an Account on <https://hub.docker.com>
* Run Hello World Container
* Run Ubuntu Server (or Other) Container with Bash
* Exit and Restart Container in Daemon Mode
* Reconnect to Named Container
* Commit Changes to Container
* Stop the Container
* Remove the Old Container

### Create and Run a Linux Virtual Machine

* Download and Install VirtualBox
* Download and Install PopOS (or Other)
* Create a VirtualBox Virtual Machine
* Boot the Virtual Machine with Media Image
* Complete the Installation and Setup

## Day 9

[📺]{.noinvert} [Linux Beginner Boost - Day 9 (May 14, 2020)](https://youtu.be/LfD6YFsYkCg)

### Find a Computer

* Linux Runs on Anything
* Look Around (No Seriously)
* Get a Raspberry Pi (Not Arduino)
* Be Sure You Can Break It!

### Create a Bootable USB Drive

* Dangers of DD Disk Destroyer
* Why Not Rufus or Other Tools 
* Download Balena Etcher, Grok Why
* Download a Linux Install Image
* Grok Live Booting
* Get a USB Thumb Drive
* Flash Image to Thumb Drive

### Understand Boot Process

* Grok BIOS
* Grok Boot Process
* Grok Boot Order
* Grok Disk Partitions
* Grok Boot Loaders
* Grok UEFI
* Grok Grub
* Grok Init Levels
* Grok Danger of Powering Off
* Research Your Computer Type
* Identify Setup Key Sequence
* Change Your Boot Order to USB First

### Know If and When to Dual Boot

* Grok Pros and Cons of Dual Booting
* Consider Mint or Easy Dual Boot

### Practice Live Booting

* Boot from a USB
* Live Boot to Install
* Live Boot to Recover
* Live Boot to Pwn (SecOps)
* Create Persistent Live Boot

### Make It Safe

* Evaluate Your Security Needs
* Pick a Secure Distro
* Update and Upgrade Immediately
* Consider How and Where Will Be Used
* Consider Disk Encryption
* Get a Wifi Dongle Just In Case
* Consider Ethernet Over Wifi
* Create a User with Sudo
* Check for Open Listeners
* Consider Backup Strategies

## Day 10

[📺]{.noinvert} [Linux Beginner Boost - Day 10 (May 15, 2020)](https://youtu.be/X0ArQfix31U)

### Use Software Package Managers

* Grok No [App Stores](/appstores/)
* Grok [What About PWA](/pwa/)
* Grok How Packages Define Distros
* Know *Your* Package Manager
* Use `dpkg` and `apt`
* Use `apt update`
* Use `apt upgrade`
* Grok PPA
* Add a PPA

### Understand Professional Linux Occupations

* Research Software Engineer
* Research DevOps Engineer
* Research Site/Service Reliability Engineer
* Prove Your Skills

### Review the Week

* Read Over Material
* Log About the Week
* Set Goals for the Weekend
* Discuss Learning Project Ideas
