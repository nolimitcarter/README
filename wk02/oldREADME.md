---
Title: Installing and Running Linux
Subtitle: Your First *Full* Linux Experience
---

:::co-tip
Are you sure you don't just want a [Linux terminal](/terminal/#get) instead?
:::

1. Where did Linux come from?
1. Why use Linux?
1. Which Linux should *I* use?
1. How can I install Linux?
    1. WSL
    1. Containers
    1. Virtual Machines
    1. Bare Metal
